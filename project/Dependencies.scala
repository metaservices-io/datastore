import sbt._

object Dependencies {

  lazy val dependencies = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.11",
    "org.slf4j" % "log4j-over-slf4j" % "1.7.36",
    "com.google.guava" % "guava" % "31.1-jre",
    "io.projectreactor.netty"  % "reactor-netty-core" % "1.1.2",
    "org.apache.commons" % "commons-lang3" % "3.12.0",
    "org.apache.spark" %% "spark-sql" % "3.3.1" excludeAll ExclusionRule("org.slf4j", "slf4j-log4j12"),
    "com.github.f4b6a3" % "uuid-creator" % "5.2.0",
    "com.github.pathikrit" %% "better-files" % "3.9.1",
    "com.google.googlejavaformat" % "google-java-format" % "1.15.0",
    "io.github.ykayacan.hashing" % "hashing-rendezvous" % "0.1.1-SNAPSHOT",
    "net.openhft" % "zero-allocation-hashing" % "0.16",
    "it.unimi.dsi" % "fastutil" % "8.5.11",
    "org.rocksdb" % "rocksdbjni" % "7.9.2",
    "com.twitter" %% "util-core" % "22.12.0"
  )

  lazy val testDependencies = Seq(
    "org.scalatest" %% "scalatest" % "3.2.15" % Test
  )

  lazy val overrides = Seq(
    "org.scala-lang.modules" %% "scala-xml" % "2.1.0"
  )

}
