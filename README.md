# metaservice-datastore

Open source distributed database build around Kafka as a distributed log and sql queryable state machines.
It is wire compatible with a subset of PostgreSQL 7.4+ protocol.
