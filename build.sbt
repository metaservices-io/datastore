import Dependencies._

ThisBuild / scalaVersion     := "2.13.7"
ThisBuild / version          := "0.1.0"
ThisBuild / organization     := "io.metaservices"
ThisBuild / organizationName := "metaservices"

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "metaservices-datastore",
    resolvers ++= Resolver.sonatypeOssRepos("snapshots"),
    libraryDependencies ++= dependencies,
    libraryDependencies ++= testDependencies,
    dependencyOverrides ++= overrides,
    coverageExcludedPackages := "<empty>;io.metaservices.datastore.Bootstrap*",
    scalacOptions ++= Seq("-unchecked", "-deprecation"),
    Test / fork := true,
    Test / parallelExecution := false
  )

ThisBuild / description := "Open source distributed database"
ThisBuild / licenses    := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
ThisBuild / homepage    := Some(url("https://gitlab.com/metaservices-io/datastore"))
// ThisBuild / scmInfo := Some(
//   ScmInfo(
//     url("https://github.com/your-account/your-project"),
//     "scm:git@github.com:your-account/your-project.git"
//   )
// )
ThisBuild / developers := List(
  Developer(
    id    = "@benschaff",
    name  = "Benjamin Schaff",
    email = "benjamin.schaff@gmail.com",
    url   = url("https://www.linkedin.com/in/benjamin-schaff-78155531")
  )
)
ThisBuild / pomIncludeRepository := { _ => false }
// ThisBuild / publishTo := {
//   val nexus = "https://oss.sonatype.org/"
//   if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
//   else Some("releases" at nexus + "service/local/staging/deploy/maven2")
// }
ThisBuild / publishMavenStyle := true
