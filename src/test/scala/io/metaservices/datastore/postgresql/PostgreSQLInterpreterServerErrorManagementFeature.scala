package io.metaservices.datastore.postgresql

import io.metaservices.datastore.postgresql.PostgreSQLInterpreterServerTestingUtils.{assertOneReceived, send, withPostgreSQLInterpreterServerClient}
import org.scalatest.GivenWhenThen
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.matchers.must.Matchers
import reactor.core.publisher.Mono

class PostgreSQLInterpreterServerErrorManagementFeature extends AnyFeatureSpec with GivenWhenThen with Matchers {

  Feature("Server protocol error management") {
    Scenario("invalid message payload") {
      Given("a running PostgreSQLInterpreterServer")
      And("a tcp client connected to the server")
      withPostgreSQLInterpreterServerClient { client =>
        When("submitting an invalid request")
        send(client.outbound(), Array[Byte](0x00))

        Then("the response must an error")
        assertOneReceived(client.inbound().receive().asByteArray())(_(0) must be (0x45))
      }
    }

// There is no unmanaged parsed message for now
//    Scenario("unsupported message") {
//      Given("a running PostgreSQLInterpreterServer")
//      And("a tcp client connected to the server")
//      withPostgreSQLInterpreterServerClient { client =>
//        val minimalStartupMessage: Array[Byte] = Array[Byte](
//          0x00, 0x00, 0x00, 0x12, // message length in bytes including size itself
//          0x00, 0x03, 0x00, 0x00, // protocol version 3.0
//          0x75, 0x73, 0x65, 0x72, 0x00, 0x72, 0x6F, 0x6F, 0x74, 0x00 // user: root
//        )
//
//        When("submitting an unsupported message")
//        client.outbound().sendByteArray(Mono.just(minimalStartupMessage)).`then`().subscribe()
//
//        Then("the response must an error")
//        client.inbound().receive().asByteArray().blockFirst() must equal (Array[Byte](
//          0x45, 0x00, 0x00, 0x00, 0x4A,
//          0x56, 0x55, 0x6E, 0x73, 0x75, 0x70, 0x70, 0x6F, 0x72, 0x74, 0x65, 0x64, 0x20, 0x6D, 0x65, 0x73, 0x73, 0x61,
//          0x67, 0x65, 0x3A, 0x20, 0x53, 0x74, 0x61, 0x72, 0x74, 0x75, 0x70, 0x4D, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
//          0x28, 0x50, 0x72, 0x6F, 0x74, 0x6F, 0x63, 0x6F, 0x6C, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x28, 0x33,
//          0x2C, 0x30, 0x29, 0x2C, 0x72, 0x6F, 0x6F, 0x74, 0x2C, 0x4E, 0x6F, 0x6E, 0x65, 0x29, 0x0, 0x0
//        ))
//      }
//    }
  }

}
