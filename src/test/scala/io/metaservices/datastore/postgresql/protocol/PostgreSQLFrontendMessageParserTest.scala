package io.metaservices.datastore.postgresql.protocol

import com.google.common.primitives.Bytes
import io.metaservices.datastore.postgresql.protocol.PostgreSQLFrontendMessageParser.{ProtocolVersion, SSLRequest, StartupMessage}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class PostgreSQLFrontendMessageParserTest extends AnyWordSpec with Matchers {

  private val minimalStartupMessage: Array[Byte] = Array[Byte](
    0x00, 0x00, 0x00, 0x12, // message length in bytes including size itself
    0x00, 0x03, 0x00, 0x00, // protocol version 3.0
    0x75, 0x73, 0x65, 0x72, 0x00, 0x72, 0x6F, 0x6F, 0x74, 0x00 // user: root
  )

  "A PostgreSQLFrontendMessageParser" when {
    "non initialized" should {
      "accept an ssl request message" in {
        PostgreSQLFrontendMessageParser()
          .parse(Array[Byte](0x00, 0x00, 0x00, 0x08, 0x04, 0xD2.toByte, 0x16, 0x2F)) must be (SSLRequest)
      }

      "accept a minimal startup message" in {
        PostgreSQLFrontendMessageParser()
          .parse(minimalStartupMessage) must be (StartupMessage(ProtocolVersion(3, 0), user = "root"))
      }

      "accept a startup message with parameters" in {
        val message = Bytes.concat(
          minimalStartupMessage,
          Array[Byte](
            0x64, 0x61, 0x74, 0x61, 0x62, 0x61, 0x73, 0x65, 0x00, 0x64, 0x62, 0x00, // database: db,
            0x6F, 0x70, 0x74, 0x69, 0x6F, 0x6E, 0x73, 0x00, 0x61, 0x72, 0x67, 0x73, 0x00, // options: args,
            0x72, 0x65, 0x70, 0x6C, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x00, 0x74, 0x72, 0x75, 0x65, 0x00 // replication: true
          )
        )

        message(3) = 0x3C

        PostgreSQLFrontendMessageParser()
          .parse(message) must be (StartupMessage(ProtocolVersion(3, 0), user = "root", database = Some("db")))
      }

      "reject an invalid ssl request message" in {
        the [IllegalArgumentException] thrownBy PostgreSQLFrontendMessageParser()
          .parse(Array[Byte](0x00, 0x00, 0x00, 0x08, 0x04, 0x02, 0x16, 0x2F)) must have message (
          "requirement failed: invalid ssl request message"
        )
      }

      "reject a startup message with an invalid total length" in {
        val invalid = Seq.from(minimalStartupMessage).toArray
        invalid(3) = 0x10

        the [IllegalArgumentException] thrownBy PostgreSQLFrontendMessageParser().parse(invalid) must have message (
          "requirement failed: the length field in the message differs from the byte array size"
        )
      }

      "reject a startup message without the user parameter" in {
        val invalid = Seq.from(minimalStartupMessage).toArray
        invalid(8) = 0x74

        the [IllegalArgumentException] thrownBy PostgreSQLFrontendMessageParser().parse(invalid) must have message (
          "requirement failed: startup message does not contain the user"
        )
      }
    }
  }

}
