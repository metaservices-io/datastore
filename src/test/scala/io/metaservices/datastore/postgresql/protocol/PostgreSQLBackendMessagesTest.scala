package io.metaservices.datastore.postgresql.protocol

import io.metaservices.datastore.postgresql.protocol.PostgreSQLBackendMessages.RichByteBuf
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class PostgreSQLBackendMessagesTest extends AnyWordSpec with Matchers {

  "A PostgreSQLBackendMessages" when {
    "creating an error message" should {
      "use the throwable message as the error message" in {
        PostgreSQLBackendMessages.errorMessage(new Throwable("error")).toByteArray must equal (
          Array[Byte](
            0x45, 0x00, 0x00, 0x00, 0x1A,
            0x53, 0x45, 0x52, 0x52, 0x4F, 0x52, 0x00,
            0x4D, 0x65, 0x72, 0x72, 0x6F, 0x72, 0x00,
            0x43, 0x58, 0x58, 0x30, 0x30, 0x30, 0x00,
            0x00
          )
        )
      }

      "use unknown error if the throwable has no message" in {
        PostgreSQLBackendMessages.errorMessage(new Throwable()).toByteArray must equal (
          Array[Byte](
            0x45, 0x00, 0x00, 0x00, 0x22,
            0x53, 0x45, 0x52, 0x52, 0x4F, 0x52, 0x00,
            0x4D, 0x75, 0x6E, 0x6B, 0x6E, 0x6F, 0x77, 0x6E, 0x20, 0x65, 0x72, 0x72, 0x6F, 0x72, 0x00,
            0x43, 0x58, 0x58, 0x30, 0x30, 0x30, 0x00,
            0x00
          )
        )
      }
    }

    "creating an ssl support message" should {
      "always return false" in {
        PostgreSQLBackendMessages.sslSupportMessage() must equal (Array[Byte](0x4E))
      }
    }
  }

}
