package io.metaservices.datastore.postgresql

import io.metaservices.datastore.postgresql.PostgreSQLInterpreterServerTestingUtils._
import io.metaservices.datastore.postgresql.protocol.PostgreSQLBackendMessages
import io.metaservices.datastore.postgresql.protocol.PostgreSQLBackendMessages.RichByteBuf
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.scalatest.GivenWhenThen
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.matchers.must.Matchers

class PostgreSQLInterpreterServerSimpleQueryFlowFeature extends AnyFeatureSpec with GivenWhenThen with Matchers {

  Feature("Simple Query Flow") {
    Scenario("simple query flow with empty response query") {
      Given("a running PostgreSQLInterpreterServer")
      And("a tcp client connected to the server")
      withPostgreSQLInterpreterServerClient { client =>
        val inboundFlux = client.inbound().receive().asByteArray().share()
        val disposable = inboundFlux.subscribe()

        val outbound = client.outbound()

        When("executing the simple query flow")
        // SSLRequest
        send(outbound, sslRequest())

        val schema = StructType(Seq(StructField(name = "namespace", DataTypes.StringType)))
        assertOneReceived(inboundFlux)(_ must be (PostgreSQLBackendMessages.sslSupportMessage()))

        // StartupMessage
        send(outbound, startupMessage())

        assertMultipleReceived(inboundFlux)(
          { _ must equal(PostgreSQLBackendMessages.authenticationOkMessage) },
          { _.slice(0, 9) must equal(PostgreSQLBackendMessages.backendKeyData(0, 0).slice(0, 9).toByteArray) },
          { _ must equal(PostgreSQLBackendMessages.clientEncodingUtf8ParameterStatusMessage) },
          { _ must equal(PostgreSQLBackendMessages.dateStyleParameterStatusMessage) },
          { _ must equal(PostgreSQLBackendMessages.readyForQuery()) }
        )

        send(outbound, Array[Byte](
          0x51, 0x00, 0x00, 0x00, 0x13, 0x73, 0x68, 0x6F, 0x77, 0x20, 0x64, 0x61, 0x74, 0x61, 0x62, 0x61, 0x73, 0x65, 0x73, 0x00
        ))

        Then("he flow must end with ReadyForQuery message")
        assertMultipleReceived(inboundFlux)(
          { _ must equal(PostgreSQLBackendMessages.rowDescription(schema).toByteArray) },
          { _ must equal(PostgreSQLBackendMessages.dataRow(schema, Row("default")).toByteArray) },
          { _ must equal(PostgreSQLBackendMessages.commandComplete(count = 1).toByteArray) },
          { _ must equal(PostgreSQLBackendMessages.readyForQuery()) }
        )

        disposable.dispose()
      }
    }
  }

}
