package io.metaservices.datastore.postgresql

import io.metaservices.datastore.postgresql.protocol.PostgreSQLBackendMessages.RichByteBuf
import io.metaservices.datastore.postgresql.protocol.PostgreSQLFrontendMessageParser
import io.netty.buffer.{ByteBuf, ByteBufAllocator}
import org.apache.spark.sql.SparkSession
import org.scalatest.matchers.must.Matchers
import reactor.core.publisher.{Flux, Mono}
import reactor.netty.tcp.TcpClient
import reactor.netty.{Connection, NettyOutbound}

import java.util

object PostgreSQLInterpreterServerTestingUtils extends Matchers {

  private val byteBufAllocator = ByteBufAllocator.DEFAULT

  lazy private val session = SparkSession
    .builder()
    .master("local[1]")
    .appName("MetaservicesDatastoreDistributedQueryEngineTest")
    .getOrCreate()

  sys.addShutdownHook(session.close())

  def withPostgreSQLInterpreterServerClient(execute: Connection => Unit): Unit = {
    withPostgreSQLInterpreterServer { server =>
      val connection = TcpClient.create().host(server.host()).port(server.port()).connectNow()
      try execute(connection)
      finally connection.disposeNow()
    }
  }

  def withPostgreSQLInterpreterServer(execute: PostgreSQLInterpreterServer => Unit): Unit = {
    val postgreSQLInterpreterServer = PostgreSQLInterpreterServer(Map.empty, session)
    postgreSQLInterpreterServer.start(blockUntilShutdown = false)
    postgreSQLInterpreterServer.start(blockUntilShutdown = false)
    try execute(postgreSQLInterpreterServer)
    finally postgreSQLInterpreterServer.stop()
  }

  def sslRequest(): ByteBuf = {
    byteBufAllocator.buffer(8).writeInt(8).writeInt(80877103)
  }

  def startupMessage(user: String = "root", database: Option[String] = None): ByteBuf = {
    val buffer = byteBufAllocator
      .buffer()
      .writeInt(0)
      .writeInt(196608)
      .writeCString("user")
      .writeCString(user)

    database.foreach(buffer.writeCString)
    buffer.writeByte(0x00)
    buffer.setInt(0, buffer.readableBytes())
  }

  def send(outbound: NettyOutbound, message: ByteBuf): Unit = {
    outbound.send(Mono.just(message)).`then`().block()
  }

  def send(outbound: NettyOutbound, message: Array[Byte]): Unit = {
    outbound.sendByteArray(Mono.just(message)).`then`().block()
  }

  def assertOneReceived(inboundFlux: Flux[Array[Byte]])(assertion: Array[Byte] => Unit): Unit = {
    assertion(inboundFlux.blockFirst())
  }

  def assertMultipleReceived(inboundFlux: Flux[Array[Byte]])(assertions: Array[Byte] => Unit*): Unit = {
    val bytes = inboundFlux.blockFirst()
    var position: Int = 0
    val iterator = unpack(bytes)
    while (iterator.hasNext) {
      val message = iterator.next()
      assertions(position)(message)

      position += 1
    }

    position must be(assertions.length)
  }

  private def unpack(bytes: Array[Byte]): Iterator[Array[Byte]] = {
    new Iterator[Array[Byte]] {
      private var offset = 0
      override def hasNext: Boolean = offset < bytes.length
      override def next(): Array[Byte] = {
        if (offset == bytes.length - 1) {
          offset += 1
          Array(bytes.last)
        } else {
          val length = PostgreSQLFrontendMessageParser.readInt(bytes, offset + 1)
          val message = util.Arrays.copyOfRange(bytes, offset, offset + length + 1)
          offset += length + 1

          message
        }
      }
    }
  }

}
