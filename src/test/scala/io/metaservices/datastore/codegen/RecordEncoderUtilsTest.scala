package io.metaservices.datastore.codegen

import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.util.DateTimeUtils
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.apache.spark.unsafe.types.{CalendarInterval, UTF8String}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

import java.nio.charset.StandardCharsets
import java.time.{Instant, LocalDate}
import java.util.Base64

class RecordEncoderUtilsTest extends AnyFlatSpec with Matchers {

  private val schema = StructType(Seq(
    StructField("aString", DataTypes.StringType),
    StructField("aBlob", DataTypes.BinaryType),
    StructField("aBoolean", DataTypes.BooleanType),
    StructField("aDate", DataTypes.DateType),
    StructField("aTimestamp", DataTypes.TimestampType),
    StructField("aCalendarInterval", DataTypes.CalendarIntervalType),
    StructField("aDouble", DataTypes.DoubleType),
    StructField("aFloat", DataTypes.FloatType),
    StructField("aByte", DataTypes.ByteType),
    StructField("anInteger", DataTypes.IntegerType),
    StructField("aLong", DataTypes.LongType),
    StructField("aShort", DataTypes.ShortType),
    StructField("aNull", DataTypes.NullType)
  ))

  private val row = InternalRow(
    UTF8String.fromString("string"),
    Base64.getEncoder.encode("blob".getBytes(StandardCharsets.UTF_8)),
    true,
    DateTimeUtils.localDateToDays(LocalDate.of(2022, 1, 1)),
    DateTimeUtils.instantToMicros(Instant.parse("2022-01-01T00:00:00Z")),
    new CalendarInterval(0, 5, 0),
    1.0d,
    2.0f,
    3.toByte,
    4,
    5L,
    6.toShort,
    null
  )

  "A RecordEncoderUtils" should "create an encoder based on a schema" in {
    val encoder = RecordEncoderUtils.generate("test", schema)
    val bytes = encoder.encode(row)
    val decoded = encoder.decode(bytes)
    decoded.getString(0) must be (row.getString(0))
    decoded.getBinary(1) must equal (row.getBinary(1))
    decoded.getBoolean(2) must be (true)
    decoded.getInt(3) must be (row.getInt(3))
    decoded.getLong(4) must be (row.getLong(4))
    decoded.getInterval(5) must equal (row.getInterval(5))
    decoded.getDouble(6) must be (row.getDouble(6))
    decoded.getFloat(7) must be (row.getFloat(7))
    decoded.getByte(8) must be (row.getByte(8))
    decoded.getInt(9) must be (row.getInt(9))
    decoded.getLong(10) must be (row.getLong(10))
    decoded.getShort(11) must be (row.getShort(11))
    decoded.isNullAt(12) must be (true)
  }

  it should "create an encoder based on a projection" in {
    val encoder = RecordEncoderUtils.generate(
      "test",
      schema,
      projections = StructType(Seq(
        StructField("aNull", DataTypes.NullType),
        StructField("aBlob", DataTypes.BinaryType),
        StructField("aTimestamp", DataTypes.TimestampType),
        StructField("aString", DataTypes.StringType)
      ))
    )

    val bytes = encoder.encode(row)
    val decoded = encoder.decode(bytes)
    decoded.isNullAt(0) must be (true)
    decoded.getBinary(1) must equal (row.getBinary(1))
    decoded.getLong(2) must be (row.getLong(4))
    decoded.getString(3) must be (row.getString(0))
  }

}
