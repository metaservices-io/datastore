package io.metaservices.datastore.store.rocksdb

import better.files.File
import com.google.common.primitives.{Bytes, Longs}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.nio.charset.StandardCharsets

class RocksDBStoreManagerTest extends AnyWordSpec with Matchers {

  private val asciiCharset = StandardCharsets.US_ASCII

  private val tableId = 0L

  "A RocksDBStoreManager" when {
    "writing entries" should {
      "support iterating over them" in {
        File.usingTemporaryDirectory() { directory =>
          RocksDBStoreManager.load(directory)
          RocksDBStoreManager
            .get()
            .write(Iterator(
              Bytes.concat(Longs.toByteArray(tableId), "key".getBytes(asciiCharset)) ->
                "value".getBytes(asciiCharset)
            ))

          RocksDBStoreManager
            .get()
            .iterator(tableId)
            .map { case (key, value) =>
              new String(key.drop(Longs.BYTES), asciiCharset) -> new String(value, asciiCharset)
            }
            .toList must be (List("key" -> "value"))
        }
      }
    }
  }
}
