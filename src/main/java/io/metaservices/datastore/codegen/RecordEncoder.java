package io.metaservices.datastore.codegen;

import org.apache.spark.sql.catalyst.InternalRow;

public abstract class RecordEncoder {
    abstract public byte[] encode(final InternalRow row);
    abstract public InternalRow decode(final byte[] bytes);
}
