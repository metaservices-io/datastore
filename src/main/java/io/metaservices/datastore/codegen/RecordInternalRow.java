package io.metaservices.datastore.codegen;

import org.apache.commons.lang.NotImplementedException;
import org.apache.spark.sql.catalyst.InternalRow;
import org.apache.spark.sql.catalyst.util.ArrayData;
import org.apache.spark.sql.catalyst.util.MapData;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.Decimal;

public abstract class RecordInternalRow extends InternalRow  {

    final protected byte[] bytes;

    public RecordInternalRow(final byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public void setNullAt(final int i) {
        throw new NotImplementedException("setNullAt(" + i + ") is not implemented");
    }

    @Override
    public void update(final int i, final Object value) {
        throw new NotImplementedException("setNullAt(" + i + ", " + value + ") is not implemented");
    }

    @Override
    public Decimal getDecimal(final int ordinal, final int precision, final int scale) {
        throw new NotImplementedException("getStruct(" + ordinal + ", " + precision + ", " + scale + ") is not implemented");
    }

    @Override
    public InternalRow getStruct(final int ordinal, final int numFields) {
        throw new NotImplementedException("getStruct(" + ordinal + ", " + numFields + ") is not implemented");
    }

    @Override
    public ArrayData getArray(final int ordinal) {
        throw new NotImplementedException("getArray(" + ordinal + ") is not implemented");
    }

    @Override
    public MapData getMap(final int ordinal) {
        throw new NotImplementedException("getMap(" + ordinal + ") is not implemented");
    }

    @Override
    public Object get(final int ordinal, final DataType dataType) {
        throw new NotImplementedException("get(" + ordinal + ", " + dataType + ") is not implemented");
    }

}
