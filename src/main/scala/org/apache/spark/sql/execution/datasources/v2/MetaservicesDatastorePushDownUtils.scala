package org.apache.spark.sql.execution.datasources.v2

import org.apache.spark.sql.catalyst.expressions.{Attribute, Expression, GetArrayStructFields, GetStructField, PredicateHelper}
import org.apache.spark.sql.connector.read.{Scan, ScanBuilder, SupportsPushDownFilters, SupportsPushDownRequiredColumns}
import org.apache.spark.sql.execution.datasources.{MetaservicesDatastoreDataSourceStrategy, PushableColumnBase}
import org.apache.spark.sql.{SparkSqlUtils, sources}

import scala.collection.mutable

object MetaservicesDatastorePushDownUtils extends PredicateHelper {
  /**
   * Pushes down filters to the data source reader
   *
   * @return pushed filter and post-scan filters.
   */
  def pushFilters(
      scanBuilder: ScanBuilder,
      filters: Seq[Expression]): (Seq[sources.Filter], Seq[Expression]) = {
    scanBuilder match {
      case r: SupportsPushDownFilters =>
        // A map from translated data source leaf node filters to original catalyst filter
        // expressions. For a `And`/`Or` predicate, it is possible that the predicate is partially
        // pushed down. This map can be used to construct a catalyst filter expression from the
        // input filter, or a superset(partial push down filter) of the input filter.
        val translatedFilterToExpr = mutable.HashMap.empty[sources.Filter, Expression]
        val translatedFilters = mutable.ArrayBuffer.empty[sources.Filter]
        // Catalyst filter expression that can't be translated to data source filters.
        val untranslatableExprs = mutable.ArrayBuffer.empty[Expression]

        for (filterExpr <- filters) {
          val translated =
            MetaservicesDatastoreDataSourceStrategy.translateFilterWithMapping(filterExpr, Some(translatedFilterToExpr))
          if (translated.isEmpty) {
            untranslatableExprs += filterExpr
          } else {
            translatedFilters += translated.get
          }
        }

        // Data source filters that need to be evaluated again after scanning. which means
        // the data source cannot guarantee the rows returned can pass these filters.
        // As a result we must return it so Spark can plan an extra filter operator.
        val postScanFilters = r.pushFilters(translatedFilters.toArray).map { filter =>
          MetaservicesDatastoreDataSourceStrategy.rebuildExpressionFromFilter(filter, translatedFilterToExpr)
        }
        (r.pushedFilters(), (untranslatableExprs ++ postScanFilters).toSeq)

      case _ => (Nil, filters)
    }
  }

  /**
   * Applies column pruning to the data source, w.r.t. the references of the given expressions.
   *
   * @return the `Scan` instance (since column pruning is the last step of operator pushdown),
   */
  def pruneColumns(scanBuilder: ScanBuilder, requiredColumn: Seq[Attribute]): Scan = {
    scanBuilder match {
      case r: SupportsPushDownRequiredColumns =>
        r.pruneColumns(SparkSqlUtils.toStructType(requiredColumn))
        r.build()

      case _ => scanBuilder.build()
    }
  }

  object PushableColumnAndNestedColumn extends PushableColumnBase {
    override val nestedPredicatePushdownEnabled = true
    override def unapply(e: Expression): Option[String] = {
      import org.apache.spark.sql.connector.catalog.CatalogV2Implicits.MultipartIdentifierHelper
      def helper(e: Expression): Option[Seq[String]] = e match {
        case a: Attribute =>
          if (nestedPredicatePushdownEnabled || !a.name.contains(".")) {
            Some(Seq(a.name))
          } else {
            None
          }
        case s: GetStructField if nestedPredicatePushdownEnabled =>
          helper(s.child).map(_ :+ s.childSchema(s.ordinal).name)
        case s: GetArrayStructFields if nestedPredicatePushdownEnabled =>
          helper(s.child).map(_ :+ s.field.name)
        case _ => None
      }
      helper(e).map(_.quoted)
    }
  }

}
