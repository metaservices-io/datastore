package org.apache.spark.sql

import org.apache.spark.sql.catalyst.expressions.Attribute
import org.apache.spark.sql.types.{StructField, StructType}

object SparkSqlUtils {

  def show(dataFrame: DataFrame, truncate: Boolean = false, rows: Int = 10): String = {
    dataFrame.showString(_numRows = rows, truncate = if (truncate) 20 else 0)
  }

  def toStructType(attributes: Seq[Attribute]): StructType = {
    StructType(attributes.map(a => StructField(a.name, a.dataType, a.nullable, a.metadata)))
  }

}
