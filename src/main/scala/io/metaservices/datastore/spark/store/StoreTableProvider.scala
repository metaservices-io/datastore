package io.metaservices.datastore.spark.store

import io.metaservices.datastore.store.StoreManager
import io.metaservices.datastore.store.StoreManager.Types
import org.apache.spark.sql.connector.catalog.{Table, TableProvider}
import org.apache.spark.sql.connector.expressions.Transform
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap

import java.util

class StoreTableProvider extends TableProvider {
  override def supportsExternalMetadata(): Boolean = true
  override def inferSchema(options: CaseInsensitiveStringMap): StructType = null
  override def getTable(schema: StructType,
                        partitioning: Array[Transform],
                        properties: util.Map[String, String]): Table = {

    val name = properties.get("name")
    val shards = properties.get("shards").toInt
    val `type` = properties.get("type")

    StoreManager.create(name, schema, shards, `type` = Types.of(`type`))
    StoreTable(name, schema, shards)
  }
}
