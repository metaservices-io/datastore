package io.metaservices.datastore.spark.store

import com.google.common.collect.Sets
import io.metaservices.datastore.spark.store.StoreTable.{StoreTableScanBuilder, StoreTableWriteBuilder}
import io.metaservices.datastore.store.StoreManager
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.connector.catalog.{SupportsRead, SupportsWrite, Table, TableCapability}
import org.apache.spark.sql.connector.read._
import org.apache.spark.sql.connector.write._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap

import java.util

class StoreTable(override val name: String,
                 override val schema: StructType,
                 val shards: Int) extends Table with SupportsRead with SupportsWrite {

  override def capabilities(): util.Set[TableCapability] = {
    Sets.newHashSet(TableCapability.BATCH_READ, TableCapability.BATCH_WRITE)
  }

  override def newScanBuilder(options: CaseInsensitiveStringMap): ScanBuilder = {
    new StoreTableScanBuilder(name, schema, shards)
  }

  override def newWriteBuilder(info: LogicalWriteInfo): WriteBuilder = new StoreTableWriteBuilder(name)

}

object StoreTable {

  def apply(name: String, schema: StructType, shards: Int): StoreTable = {
    new StoreTable(name, schema, shards)
  }

  case object StoreTableWriterCommitMessage extends WriterCommitMessage
  private class StoreTableWriteBuilder(name: String) extends WriteBuilder {
    override def build(): Write = new Write {
      override def toBatch: BatchWrite = new BatchWrite {
        override def commit(messages: Array[WriterCommitMessage]): Unit = ()
        override def abort(messages: Array[WriterCommitMessage]): Unit = ()
        override def createBatchWriterFactory(info: PhysicalWriteInfo): DataWriterFactory = {
          new StoreTableDataWriterFactory(name)
        }
      }
    }
  }

  private class StoreTableDataWriterFactory(name: String) extends DataWriterFactory {
    override def createWriter(partitionId: Int, taskId: Long): DataWriter[InternalRow] = new DataWriter[InternalRow] {
      private val Some(store) = StoreManager.get(name)
      private val buffer = Array.ofDim[InternalRow](1024)
      private var position = 0
      override def write(record: InternalRow): Unit = {
        buffer(position) = record
        position += 1
        if (position == buffer.length) {
          store.insert(buffer.iterator)
          position = 0
        }
      }
      override def commit(): WriterCommitMessage = {
        store.insert(buffer.take(position).iterator)
        StoreTableWriterCommitMessage
      }

      override def abort(): Unit = ()
      override def close(): Unit = ()
    }
  }

  case class StoreTableInputPartition(shard: Int, projections: StructType) extends InputPartition
  private class StoreTableScanBuilder(name: String, schema: StructType, shards: Int) extends ScanBuilder with SupportsPushDownRequiredColumns {
    private var required: StructType = schema
    override def build(): Scan = new Scan {
      override def readSchema(): StructType = schema
      override def toBatch: Batch = new Batch {
        override def planInputPartitions(): Array[InputPartition] = {
          (0 until shards).map(StoreTableInputPartition(_, required)).toArray
        }

        override def createReaderFactory(): PartitionReaderFactory = new StoreTablePartitionReaderFactory(name)
      }
    }

    override def pruneColumns(requiredSchema: StructType): Unit = {
      required = requiredSchema
    }
  }

  private class StoreTablePartitionReaderFactory(name: String) extends PartitionReaderFactory {
    override def createReader(partition: InputPartition): PartitionReader[InternalRow] = new PartitionReader[InternalRow] {
      private val Some(store) = StoreManager.get(name)
      private val iterator = store.scan(
        partition.asInstanceOf[StoreTableInputPartition].shard,
        partition.asInstanceOf[StoreTableInputPartition].projections
      )

      override def next(): Boolean = iterator.hasNext
      override def get(): InternalRow = iterator.next()
      override def close(): Unit = iterator.close()
    }
  }

}