package io.metaservices.datastore.spark.store

import io.metaservices.datastore.spark.TableProviderRelation
import io.metaservices.datastore.store.Store
import org.apache.spark.sql.sources.{BaseRelation, CreatableRelationProvider, DataSourceRegister, SchemaRelationProvider}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SQLContext, SaveMode}

import scala.jdk.CollectionConverters.MapHasAsJava

class DefaultSource extends SchemaRelationProvider with DataSourceRegister with CreatableRelationProvider {

  private val provider: StoreTableProvider = new StoreTableProvider

  override def shortName(): String = "store"

  override def createRelation(sqlContext: SQLContext, parameters: Map[String, String], schema: StructType): BaseRelation = {
    TableProviderRelation(provider.getTable(Store.toStoreSchema(schema), partitioning = Array.empty, properties = parameters.asJava), schema, sqlContext)
  }

  override def createRelation(sqlContext: SQLContext, mode: SaveMode, parameters: Map[String, String], data: DataFrame): BaseRelation = {
    val schema = Store.toStoreSchema(data.schema)
    val relation = TableProviderRelation(provider.getTable(schema, partitioning = Array.empty, properties = parameters.asJava), schema, sqlContext)
    relation.insert(data, overwrite = false)
    relation
  }

}
