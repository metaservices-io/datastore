package io.metaservices.datastore.spark.session

import org.apache.spark.sql.connector.catalog.{Table, TableProvider}
import org.apache.spark.sql.connector.expressions.Transform
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.apache.spark.sql.util.CaseInsensitiveStringMap

import java.util

class SessionTableProvider extends TableProvider {

  override def inferSchema(options: CaseInsensitiveStringMap): StructType = {
    StructType(Seq(
      StructField("process_id", DataTypes.IntegerType, nullable = false),
      StructField("user", DataTypes.StringType, nullable = false),
      StructField("database", DataTypes.StringType, nullable = false),
      StructField("ip", DataTypes.StringType, nullable = false),
      StructField("started_at", DataTypes.TimestampType, nullable = false),
      StructField("state", DataTypes.StringType, nullable = false),
      StructField("last_command_at", DataTypes.TimestampType, nullable = false)
    ))
  }

  override def getTable(schema: StructType,
                        partitioning: Array[Transform],
                        properties: util.Map[String, String]): Table = new SessionTable(schema)

}
