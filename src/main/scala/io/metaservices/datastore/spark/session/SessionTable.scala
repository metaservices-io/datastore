package io.metaservices.datastore.spark.session

import com.google.common.collect.Sets
import io.metaservices.datastore.postgresql.PostgreSQLInterpreterServerSession
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.util.DateTimeUtils
import org.apache.spark.sql.connector.catalog.{SupportsRead, Table, TableCapability}
import org.apache.spark.sql.connector.read.{LocalScan, Scan, ScanBuilder, SupportsPushDownRequiredColumns}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap
import org.apache.spark.unsafe.types.UTF8String

import java.util

class SessionTable(override val schema: StructType) extends Table with SupportsRead {
  override def name(): String = "sessions"
  override def capabilities(): util.Set[TableCapability] = Sets.newHashSet(TableCapability.BATCH_READ)
  override def newScanBuilder(options: CaseInsensitiveStringMap): ScanBuilder = {
    new ScanBuilder with SupportsPushDownRequiredColumns {
      private var tableSchema = schema
      override def pruneColumns(requiredSchema: StructType): Unit = {
        tableSchema = requiredSchema
      }

      override def build(): Scan = new LocalScan {
        override def readSchema(): StructType = tableSchema
        override def rows(): Array[InternalRow] = {
          PostgreSQLInterpreterServerSession.list().map { case (id, session) =>
            InternalRow.fromSeq(
              tableSchema.map { field =>
                field.name match {
                  case "process_id" => id
                  case "user" => UTF8String.fromString(session.userName())
                  case "database" => UTF8String.fromString(session.database())
                  case "ip" => UTF8String.fromString(session.ip())
                  case "started_at" => DateTimeUtils.millisToMicros(session.startedAt)
                  case "state" => UTF8String.fromString(session.state)
                  case "last_command_at" => DateTimeUtils.millisToMicros(session.lastCommandAt())
                }
              }
            )
          }.toArray
        }
      }
    }
  }
}
