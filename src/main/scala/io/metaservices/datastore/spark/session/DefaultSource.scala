package io.metaservices.datastore.spark.session

import io.metaservices.datastore.spark.TableProviderRelation
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.sources.{BaseRelation, DataSourceRegister, RelationProvider}
import org.apache.spark.sql.util.CaseInsensitiveStringMap

import scala.jdk.CollectionConverters.MapHasAsJava

class DefaultSource extends RelationProvider with DataSourceRegister {

  private val provider: SessionTableProvider = new SessionTableProvider

  private val schema = provider.inferSchema(CaseInsensitiveStringMap.empty())

  override def shortName(): String = "metadata"

  override def createRelation(sqlContext: SQLContext, parameters: Map[String, String]): BaseRelation = {
    TableProviderRelation(provider.getTable(schema, partitioning = Array.empty, properties = parameters.asJava), schema, sqlContext)
  }

}
