package io.metaservices.datastore.spark

import io.metaservices.datastore.spark.store.StoreTable
import io.metaservices.datastore.store.{ShardRouter, Store}
import org.apache.spark.TaskContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.{Attribute, Expression}
import org.apache.spark.sql.connector.catalog.{SupportsRead, SupportsWrite, Table}
import org.apache.spark.sql.connector.read.{LocalScan, ScanBuilder}
import org.apache.spark.sql.connector.write.{LogicalWriteInfo, WriteBuilder}
import org.apache.spark.sql.execution.datasources.v2.{DataSourceRDD, MetaservicesDatastorePushDownUtils}
import org.apache.spark.sql.execution.metric.SQLMetrics
import org.apache.spark.sql.internal.SQLConf
import org.apache.spark.sql.sources.{BaseRelation, CatalystScan, InsertableRelation}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap
import org.apache.spark.sql.{DataFrame, Row, SQLContext}

import java.util.UUID
import scala.collection.immutable.ArraySeq

class TableProviderRelation(val table: Table,
                            override val sqlContext: SQLContext,
                            override val schema: StructType) extends BaseRelation
                                                             with CatalystScan
                                                             with InsertableRelation { relation =>

  lazy private val leafNodeDefaultParallelism: Int = {
    sqlContext
      .sparkSession
      .conf
      .getOption(SQLConf.LEAF_NODE_DEFAULT_PARALLELISM.key)
      .map(_.toInt)
      .getOrElse(sqlContext.sparkContext.defaultParallelism)
  }

  override def needConversion: Boolean = false

  override def buildScan(requiredColumns: Seq[Attribute], filters: Seq[Expression]): RDD[Row] = {
    val scanBuilder: ScanBuilder = {
      table match {
        case supportsRead: SupportsRead => supportsRead.newScanBuilder(CaseInsensitiveStringMap.empty())
      }
    }

    MetaservicesDatastorePushDownUtils.pushFilters(scanBuilder, filters)
    val scan = MetaservicesDatastorePushDownUtils.pruneColumns(scanBuilder, requiredColumns)
    val rows: RDD[InternalRow] = {
      scan match {
        case localScan: LocalScan =>
          val rows = localScan.rows()
          val numSlices = math.min(rows.length, leafNodeDefaultParallelism)
          sqlContext.sparkContext.parallelize(ArraySeq.unsafeWrapArray(rows), numSlices)

        case _ =>
          val batch = scan.toBatch
          new DataSourceRDD(
            sc = sqlContext.sparkContext,
            inputPartitions = Seq(batch.planInputPartitions()),
            partitionReaderFactory = batch.createReaderFactory(),
            columnarReads = false,
            customMetrics = Map("numOutputRows" -> SQLMetrics.createMetric(sqlContext.sparkContext, "number of output rows"))
          )
      }
    }

    rows.asInstanceOf[RDD[Row]]
  }

  override def insert(data: DataFrame, overwrite: Boolean): Unit = {
    val inputSchema = data.schema
    val writeBuilder: WriteBuilder = {
      table match {
        case supportsWrite: SupportsWrite => supportsWrite.newWriteBuilder(new LogicalWriteInfo {
          override def options(): CaseInsensitiveStringMap = CaseInsensitiveStringMap.empty()
          override def queryId(): String = UUID.randomUUID().toString
          override def schema(): StructType = inputSchema
        })
      }
    }

    val write = writeBuilder.build()
    val batch = write.toBatch
    val shards = table.asInstanceOf[StoreTable].shards
    val factory = batch.createBatchWriterFactory(() => shards)

    data.foreachPartition { rows: Iterator[Row] =>
      val shardRouter = ShardRouter(shards)
      val encoder = RowEncoder(inputSchema).createSerializer()
      val taskContext = TaskContext.get()
      val writer = factory.createWriter(taskContext.partitionId(), taskContext.taskAttemptId())
      rows.foreach { row => writer.write(Store.toStoreRow(encoder(row), inputSchema, shardRouter)) }
      writer.commit()
      writer.close()
    }
  }

}

object TableProviderRelation {
  def apply(table: Table, schema: StructType, sqlContext: SQLContext): TableProviderRelation = {
    new TableProviderRelation(table, sqlContext, schema)
  }
}
