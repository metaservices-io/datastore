package io.metaservices.datastore.postgresql

import io.metaservices.datastore.postgresql.PostgreSQLInterpreterServer.{PostgreSQLTCPServerConfiguration, logger}
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory
import reactor.netty.DisposableServer
import reactor.netty.tcp.TcpServer

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Promise}

class PostgreSQLInterpreterServer(configuration: PostgreSQLTCPServerConfiguration, session: SparkSession) {

  private val tcpServer = TcpServer
    .create()
    .host(configuration.host)
    .port(configuration.port)
    .doOnConnection(connection => PostgreSQLInterpreterServerSession.apply(connection, session.newSession()))

  private var disposableServer: Option[DisposableServer] = None

  private val runningPromise = Promise[Unit]()

  def start(blockUntilShutdown: Boolean = true): Unit = {
    logger.info(s"starting PostgreSQLInterpreterServer")
    disposableServer = Some(tcpServer.bindNow())
    logger.info(s"PostgreSQLInterpreterServer started on ${disposableServer.get.host()}:${disposableServer.get.port()}")

    // $COVERAGE-OFF$Only used in Bootstrap class when running as a server
    if (blockUntilShutdown) Await.result(runningPromise.future, Duration.Inf)
    // $COVERAGE-ON$
  }

  def stop(): Unit = {
    logger.info(s"stopping PostgreSQLInterpreterServer")
    disposableServer.foreach(_.disposeNow())
    runningPromise.success(())
    logger.info(s"PostgreSQLInterpreterServer is stopped")
  }

  def host(): String = {
    disposableServer.map(_.host()).getOrElse(configuration.host)
  }

  def port(): Int = {
    disposableServer.map(_.port()).getOrElse(configuration.port)
  }

}

object PostgreSQLInterpreterServer {

  private val logger = LoggerFactory.getLogger(classOf[PostgreSQLInterpreterServer])

  case class PostgreSQLTCPServerConfiguration(host: String, port: Int)
  def apply(environment: Map[String, String], session: SparkSession): PostgreSQLInterpreterServer = {
    session.sql("CREATE TABLE IF NOT EXISTS sessions USING metadata")
    new PostgreSQLInterpreterServer(
      PostgreSQLTCPServerConfiguration(
        host = environment.getOrElse("METASERVICES_POSTGRESQL_INTERPRETER_SERVER_HOST", "localhost"),
        port = environment.getOrElse("METASERVICES_POSTGRESQL_INTERPRETER_SERVER_PORT", "0").toInt
      ),
      session
    )
  }

}
