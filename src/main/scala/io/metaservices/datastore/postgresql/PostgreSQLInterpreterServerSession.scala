package io.metaservices.datastore.postgresql

import io.metaservices.datastore.postgresql.PostgreSQLInterpreterServerSession.logger
import io.metaservices.datastore.postgresql.protocol.PostgreSQLFrontendMessageParser._
import io.metaservices.datastore.postgresql.protocol.{PostgreSQLBackendMessages, PostgreSQLFrontendMessageParser}
import io.netty.buffer.{ByteBuf, Unpooled}
import org.apache.spark.sql.catalyst.plans.logical.CreateTable
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{Row, SparkSession, SparkSqlUtils}
import org.slf4j.LoggerFactory
import reactor.core.publisher.{Mono, Sinks}
import reactor.netty.Connection

import java.util.Locale
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.concurrent.TrieMap
import scala.util.Random

class PostgreSQLInterpreterServerSession(private val connection: Connection,
                                         private val session: SparkSession,
                                         private val id: Int,
                                         private val secretKey: Int) {

  private val postgreSQLFrontEndMessageParser = PostgreSQLFrontendMessageParser()

  private val inbound = connection.inbound()

  private val outbound = Sinks.many().unicast().onBackpressureBuffer[ByteBuf]()

  private val subscription = connection.outbound().send(outbound.asFlux()).`then`().subscribe()

  private val consumerDisposable = inbound.receive().asByteArray().subscribe { (message: Array[Byte]) =>
    handle(message)
  }

  connection.onDispose(consumerDisposable)

  private var user = ""

  def userName(): String = user

  def database(): String = session.catalog.currentDatabase

  def ip(): String = connection.channel().remoteAddress().toString.replace("/", "")

  val startedAt: Long = System.currentTimeMillis()

  val state: String = "idle"

  def lastCommandAt(): Long = System.currentTimeMillis()

  private def handle(message: Array[Byte]): Unit = {
    try {
      val parsed = postgreSQLFrontEndMessageParser.parse(message)
      if (logger.isDebugEnabled) logger.debug(s"received message: $parsed")

      parsed match {
        case SSLRequest => write(PostgreSQLBackendMessages.sslSupportMessage())
        case StartupMessage(ProtocolVersion(3, 0), userName, database) =>
          user = userName
          database match {
            case None | Some("default") | Some(`userName`) => ()
            case Some(name) => session.sql(s"USE $name")
          }

          write(PostgreSQLBackendMessages.authenticationOkMessage)
          write(PostgreSQLBackendMessages.backendKeyData(id, secretKey))
          write(PostgreSQLBackendMessages.clientEncodingUtf8ParameterStatusMessage)
          write(PostgreSQLBackendMessages.dateStyleParameterStatusMessage)
          write(PostgreSQLBackendMessages.readyForQuery())

        case Query(command) if !PostgreSQLInterpreterServerSession.unsupportedOperations.contains(command.toUpperCase(Locale.US)) =>
          val sql = {
            val plan = session.sessionState.sqlParser.parsePlan(command)
            plan match {
              case create: CreateTable if create.tableSpec.provider.contains("store") =>
                s"""
                   |CREATE TABLE ${create.tableName.name()}(${create.tableSchema.toDDL})
                   |USING store
                   |OPTIONS(
                   |  name "${create.tableName.name()}",
                   |  shards ${create.tableSpec.options("shards")},
                   |  type "${create.tableSpec.options("type")}"
                   |)
                 """.stripMargin
              case _ => command
            }
          }

          val df = session.sql(sql)
          val schema = df.schema

          if (logger.isDebugEnabled) {
            logger.debug(s"query: $sql")
            logger.debug(s"\n${schema.treeString}")
            logger.debug(s"\n${SparkSqlUtils.show(df)}")
          }

          write(schema)
          val count = write(schema, df.collect().iterator)
          write(PostgreSQLBackendMessages.commandComplete(count))
          write(PostgreSQLBackendMessages.readyForQuery())

        case Query(_) =>
          write(PostgreSQLBackendMessages.commandComplete(0))
          write(PostgreSQLBackendMessages.readyForQuery())

        case Terminate =>
          outbound.tryEmitComplete()
          subscription.dispose()
          connection.disposeNow()
          PostgreSQLInterpreterServerSession.remove(id)
      }
    } catch {
      case throwable: Throwable =>
        logger.error("an error occurred while parsing the frontend message. see logs for more details", throwable)
        write(PostgreSQLBackendMessages.errorMessage(throwable))
        write(PostgreSQLBackendMessages.readyForQuery())
    }
  }

  private def write(message: Array[Byte]): Unit = send(Unpooled.wrappedBuffer(message))

  private def write(message: ByteBuf): Unit = send(message)

  private def write(schema: StructType): Unit = send(PostgreSQLBackendMessages.rowDescription(schema))

  private def write(schema: StructType, rows: Iterator[Row]): Int = {
    var count = 0
    rows.foreach { row =>
      send(PostgreSQLBackendMessages.dataRow(schema, row))
      count += 1
    }

    count
  }

  private def send(buffer: ByteBuf): Unit = {
    outbound.tryEmitNext(buffer)
  }

}

object PostgreSQLInterpreterServerSession {

  private val logger = LoggerFactory.getLogger(classOf[PostgreSQLInterpreterServerSession])

  private val sessions: TrieMap[Int, PostgreSQLInterpreterServerSession] = TrieMap.empty

  private val idGenerator: AtomicInteger = new AtomicInteger(0)

  private val reclaimed = new ConcurrentLinkedQueue[Int]()

  private val unsupportedOperations: Set[String] = Set(
    "BEGIN", "COMMIT", "ROLLBACK", "START TRANSACTION"
  )

  def apply(connection: Connection, session: SparkSession): PostgreSQLInterpreterServerSession = {
    val id = nextSessionId()
    val userSession = new PostgreSQLInterpreterServerSession(connection, session, id, Random.nextInt())
    sessions += id -> userSession
    userSession
  }

  def list(): Iterable[(Int, PostgreSQLInterpreterServerSession)] = sessions.toSeq

  def remove(id: Int): Unit = {
    sessions.remove(id)
    reclaimed.add(id)
  }

  private def nextSessionId(): Int = {
    if (reclaimed.isEmpty) idGenerator.getAndIncrement()
    else reclaimed.poll()
  }

}
