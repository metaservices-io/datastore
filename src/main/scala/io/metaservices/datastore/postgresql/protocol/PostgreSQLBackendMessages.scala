package io.metaservices.datastore.postgresql.protocol

import com.google.common.primitives.Ints
import io.netty.buffer.{ByteBuf, ByteBufAllocator, ByteBufUtil}
import org.apache.spark.sql.Row
import org.apache.spark.sql.types._

import java.nio.charset.StandardCharsets
import java.time.format.DateTimeFormatter
import java.util.Base64

object PostgreSQLBackendMessages {

  implicit class RichByteBuf(private val byteBuf: ByteBuf) extends AnyVal {
    def toByteArray: Array[Byte] = ByteBufUtil.getBytes(byteBuf)
    def writeCString(string: String): ByteBuf = {
      ByteBufUtil.writeUtf8(byteBuf, string)
      byteBuf.writeByte(0x00)
      byteBuf
    }
  }

  private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

  private val byteBufAllocator = ByteBufAllocator.DEFAULT

  private val terminationMarker = 0x00

  private val errorMessageFlag = 'E'

  private val unlocalizedSeverityFlag = 'S'

  private val unlocalizedErrorSeverity = "ERROR"

  private val internalErrorMessageFlag = 'C'

  private val internalErrorMessage = "XX000"

  private val errorMessageMessageFlag = 'M'

  private val sslDisabled = byteBufAllocator.buffer(1).writeByte('N').toByteArray

  private val authenticationOkFlag = 'R'

  private val readyForQueryIdleMessage = byteBufAllocator
    .buffer(6)
    .writeByte('Z')
    .writeInt(5)
    .writeByte('I')
    .toByteArray

  private val rowDescriptionFlag = 'T'

  private val dataRowFlag = 'D'

  private val commandCompleteFlag = 'C'

  private val backendKeyDataBaseMessage = byteBufAllocator
    .buffer(12)
    .writeByte('K')
    .writeInt(12)

  def errorMessage(throwable: Throwable): ByteBuf = {
    // 'E' length 'S' "ERROR" 'C' "XX000" 'M' [Throwable] NUL
    val message = encode(throwable)
    val length = Ints.BYTES + 7 + unlocalizedErrorSeverity.length + internalErrorMessage.length + message.length
    byteBufAllocator
      .buffer(length + 1)
      .writeByte(errorMessageFlag)
      .writeInt(length)
      .writeByte(unlocalizedSeverityFlag)
      .writeCString(unlocalizedErrorSeverity)
      .writeByte(errorMessageMessageFlag)
      .writeCString(message)
      .writeByte(internalErrorMessageFlag)
      .writeCString(internalErrorMessage)
      .writeByte(terminationMarker)
  }

  def sslSupportMessage(): Array[Byte] = sslDisabled

  val authenticationOkMessage: Array[Byte] = byteBufAllocator
    .buffer(9)
    .writeByte(authenticationOkFlag)
    .writeInt(8)
    .writeInt(0)
    .toByteArray

  def readyForQuery(status: Char = 'I'): Array[Byte] = {
    status match {
      case 'I' => readyForQueryIdleMessage
    }
  }

  val emptyQueryResponse: Array[Byte] = byteBufAllocator
    .buffer(5)
    .writeByte('I')
    .writeInt(4)
    .toByteArray

  def rowDescription(schema: StructType): ByteBuf = {
    val buffer = byteBufAllocator
      .buffer()
      .writeByte(rowDescriptionFlag)
      .writeInt(0)
      .writeShort(schema.length.toShort)

    schema.foreach { field =>
      buffer
        .writeCString(field.name)
        .writeInt(0)
        .writeShort(0)
        .writeInt(0)
        .writeShort(postgreSQLTypeLength(field))
        .writeInt(-1)
        .writeShort(0)
    }

    buffer.setInt(1, buffer.readableBytes() - 1)
    buffer
  }

  def dataRow(schema: StructType, row: Row): ByteBuf = {
    val buffer = byteBufAllocator
      .buffer()
      .writeByte(dataRowFlag)
      .writeInt(0)
      .writeShort(row.length.toShort)

    schema.fields.zipWithIndex.foreach { case (field, index) =>
      appendPostgreSQLTypeLengthAndValue(buffer, row, field, index)
    }

    buffer.setInt(1, buffer.readableBytes() - 1)
    buffer
  }

  def commandComplete(count: Int, operation: String = "SELECT"): ByteBuf = {
    val buffer = byteBufAllocator
      .buffer()
      .writeByte(commandCompleteFlag)
      .writeInt(0)
      .writeCString(s"$operation $count")

    buffer.setInt(1, buffer.readableBytes() - 1)
    buffer
  }

  def backendKeyData(processId: Int, secretKey: Int): ByteBuf = {
    val buffer = backendKeyDataBaseMessage
      .copy()
      .writeInt(processId)
      .writeInt(secretKey)

    buffer
  }

  val clientEncodingUtf8ParameterStatusMessage: Array[Byte] = byteBufAllocator
    .buffer(26)
    .writeByte('S')
    .writeInt(25)
    .writeCString("client_encoding")
    .writeCString("UTF8")
    .toByteArray

  val dateStyleParameterStatusMessage: Array[Byte] = byteBufAllocator
    .buffer(19)
    .writeByte('S')
    .writeInt(18)
    .writeCString("DateStyle")
    .writeCString("ISO")
    .toByteArray

  private def encode(throwable: Throwable): String = {
    Option(throwable.getMessage).getOrElse("unknown error")
  }

  private def postgreSQLTypeLength(field: StructField): Short = {
    field.dataType match {
      case DataTypes.BooleanType | DataTypes.ByteType => 1
      case DataTypes.ShortType => 2
      case DataTypes.IntegerType | DataTypes.FloatType | DataTypes.DateType => 4
      case DataTypes.LongType | DataTypes.DoubleType | DataTypes.TimestampType => 8
      case DataTypes.BinaryType | DataTypes.StringType=> -1
      case _: ArrayType => ???
      case _: MapType => ???
      case _: StructType => ???
      case DataTypes.NullType => ???
    }
  }

  private def appendPostgreSQLTypeLengthAndValue(byteBuf: ByteBuf, row: Row, field: StructField, index: Int): Unit = {
    if (row.isNullAt(index)) byteBuf.writeInt(-1)
    else {
      def writeBytes(bytes: Array[Byte]): Unit = {
        byteBuf.writeInt(bytes.length).writeBytes(bytes)
      }

      field.dataType match {
        case DataTypes.BooleanType => writeBytes(row.getBoolean(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.ByteType => writeBytes(row.getByte(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.ShortType => writeBytes(row.getShort(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.IntegerType => writeBytes(row.getInt(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.FloatType => writeBytes(row.getFloat(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.LongType => writeBytes(row.getLong(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.DoubleType => writeBytes(row.getDouble(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.DateType => writeBytes(row.getDate(index).toString.getBytes(StandardCharsets.US_ASCII))
        case DataTypes.TimestampType =>
          writeBytes(
            row
              .getTimestamp(index)
              .toLocalDateTime
              .format(dateTimeFormatter)
              .getBytes(StandardCharsets.US_ASCII)
          )

        case DataTypes.BinaryType => writeBytes(Base64.getEncoder.encode(row.getAs[Array[Byte]](index)))
        case DataTypes.StringType => writeBytes(row.getString(index).getBytes("UTF-8"))
        case _: ArrayType => ???
        case _: MapType => ???
        case _: StructType => ???
      }
    }
  }

}
