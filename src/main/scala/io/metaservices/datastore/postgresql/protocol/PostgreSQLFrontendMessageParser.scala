package io.metaservices.datastore.postgresql.protocol

import com.google.common.primitives.{Ints, Longs, Shorts}
import io.metaservices.datastore.postgresql.protocol.PostgreSQLFrontendMessageParser._

import java.nio.charset.StandardCharsets
import scala.collection.concurrent.TrieMap
import scala.collection.mutable.ArrayBuffer

class PostgreSQLFrontendMessageParser {

  private var initialized: Boolean = false

  def parse(message: Array[Byte]): PostgreSQLFrontEndProtocolMessage = {
    if (initialized) parseMessage(message)
    else {
      validateMessageLength(message)

      if (message.length == Longs.BYTES) {
        require(readShort(message, Ints.BYTES) == 1234 && readShort(message, Ints.BYTES + Shorts.BYTES) == 5679, "invalid ssl request message")
        SSLRequest
      } else {
        val startupMessage = parseStartupMessage(message)
        initialized = true

        startupMessage
      }
    }
  }

}

object PostgreSQLFrontendMessageParser {

  case class ProtocolVersion(major: Short, minor: Short)

  sealed trait PostgreSQLFrontEndProtocolMessage
  case object SSLRequest extends PostgreSQLFrontEndProtocolMessage
  case class StartupMessage(protocolVersion: ProtocolVersion,
                            user: String,
                            database: Option[String] = None) extends PostgreSQLFrontEndProtocolMessage

  case class Query(command: String) extends PostgreSQLFrontEndProtocolMessage
  case object Terminate extends PostgreSQLFrontEndProtocolMessage

  def apply(): PostgreSQLFrontendMessageParser = new PostgreSQLFrontendMessageParser

  private def validateMessageLength(message: Array[Byte], offset: Int = 0): Unit = {
    val length = readInt(message, offset)
    require(message.length - offset == length, "the length field in the message differs from the byte array size")
  }

  private def parseStartupMessage(message: Array[Byte]): PostgreSQLFrontEndProtocolMessage = {
    var offset = Ints.BYTES
    val protocolVersion = parseProtocolVersion(message, offset)
    offset += Ints.BYTES

    val parameters = TrieMap.empty[String, String]
    while (offset < message.length) {
      val (key, keyBytes) = readString(message, offset)
      val (value, valueBytes) = readString(message, offset + keyBytes)

      parameters += key -> value
      offset += keyBytes + valueBytes
    }

    require(parameters.contains("user"), "startup message does not contain the user")
    StartupMessage(protocolVersion, user = parameters("user"), database = parameters.get("database"))
  }

  private def parseMessage(message: Array[Byte]): PostgreSQLFrontEndProtocolMessage = {
    validateMessageLength(message, offset = 1)
    val contentOffset = Ints.BYTES + 1
    message(0).toChar match {
      case 'Q' => Query(command = new String(message, contentOffset, message.length - contentOffset - 1, StandardCharsets.UTF_8))
      case 'X' => Terminate
      case flag => throw new IllegalArgumentException(s"unsupported message flag: $flag")
    }
  }

  private def parseProtocolVersion(message: Array[Byte], offset: Int): ProtocolVersion = {
    ProtocolVersion(major = readShort(message, offset), minor = readShort(message, offset + java.lang.Short.BYTES))
  }

  private[postgresql] def readInt(message: Array[Byte], offset: Int): Int = {
    Ints.fromBytes(message(offset), message(offset + 1), message(offset + 2), message(offset + 3))
  }

  private def readShort(message: Array[Byte], offset: Int): Short = {
    Shorts.fromBytes(message(offset), message(offset + 1))
  }

  private def readString(message: Array[Byte], offset: Int): (String, Int) = {
    val buffer = ArrayBuffer.empty[Byte]
    var index = offset
    while (index < message.length && message(index) != 0x00) {
      buffer += message(index)
      index += 1
    }

    new String(buffer.toArray, StandardCharsets.UTF_8) -> (index - offset + 1)
  }

}
