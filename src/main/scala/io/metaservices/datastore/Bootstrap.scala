package io.metaservices.datastore

import better.files.File
import io.metaservices.datastore.postgresql.PostgreSQLInterpreterServer
import io.metaservices.datastore.store.StoreManager
import io.metaservices.datastore.store.rocksdb.RocksDBStoreManager
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object Bootstrap extends App {

  private val logger = LoggerFactory.getLogger("io.metaservices.datastore.Bootstrap")

  private val session = SparkSession
    .builder()
    .master("local[*]")
    .appName("MetaservicesDatastoreDistributedQueryEngine")
    .config("spark.scheduler.mode", "FAIR")
    .config("spark.executor.extraJavaOptions", "-XX:+UseG1GC -XX:+UseStringDeduplication")
    .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .config("spark.kryoserializer.buffer.max", "2047m")
    .config("spark.ui.enabled", "true")
    .config("spark.ui.showConsoleProgress", "true")
    .config("spark.ui.retainedJobs", "100")
    .config("spark.ui.retainedStages", "1000")
    .config("spark.ui.retainedTasks", "100000")
    .config("spark.sql.catalogImplementation", "in-memory")
    .config("spark.sql.caseSensitive", "false")
    .config("spark.sql.inMemoryColumnarStorage.compressed", "true")
    .config("spark.sql.cbo.enabled", "true")
    .config("spark.sql.statistics.histogram.enabled", "true")
    .config("spark.sql.warehouse.dir", ".meta-store")
    .config("spark.sql.adaptive.localShuffleReader.enabled", "true")
    .config("spark.sql.autoBroadcastJoinThreshold", "52428800")
    .getOrCreate()

  val stores = RocksDBStoreManager
    .load(directory = File(sys.env("METASERVICES_DATASTORE_DIRECTORY")))
    .groupBy(_.name.split("__").head)

  stores
    .foreach { case (_, shards) =>
      val store = StoreManager.load(shards)
      session.sql(
        s"""
           |CREATE TABLE ${store.name}(${store.originalSchema.toDDL})
           |USING store
           |OPTIONS(
           |  name "${store.name}",
           |  shards ${store.stores.length},
           |  type "${store.`type`.name}"
           |)
         """.stripMargin
      )
    }

  private val postgreSQLInterpreterServer = PostgreSQLInterpreterServer(sys.env, session)
  try {
    postgreSQLInterpreterServer.start()
    sys.addShutdownHook {
      postgreSQLInterpreterServer.stop()
      session.close()
    }
  } catch {
    case throwable: Throwable =>
      logger.error("an error occurred while starting metaservices-datastore. see logs for more details", throwable)
  }

}
