package io.metaservices.datastore.store

import com.github.f4b6a3.uuid.UuidCreator
import io.metaservices.datastore.store.Store.{MetadataColumns, Store, StoreIterator}
import io.metaservices.datastore.store.StoreManager.Types
import io.metaservices.datastore.store.rocksdb.RocksDBStoreManager
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.types.{DataType, DataTypes, StructField, StructType}

import java.io.Closeable
import java.util.UUID
import scala.jdk.CollectionConverters.{IteratorHasAsScala, MapHasAsJava}

class ShardedStore(val stores: Array[Store]) {

  val name: String = stores.head.name.split("__").head

  val schema: StructType = stores.head.schema

  val originalSchema: StructType = StructType(schema.filter {
    case StructField(MetadataColumns.ShardId.Name, _, _, _) => false
    case StructField(MetadataColumns.RowId.Name, _, _, _) => false
    case _ => true
  })

  val `type`: Types.Type = stores.head.`type`

  def insert(row: InternalRow): Unit = stores(row.getInt(MetadataColumns.ShardId.Ordinal)).insert(row)

  def insert(rows: Iterator[InternalRow]): Unit = {
    rows.toSeq.groupBy(_.getInt(MetadataColumns.ShardId.Ordinal)).foreach { case (shardId, rows) =>
      stores(shardId).insert(rows.iterator)
    }
  }

  def scan(shard: Int, projections: StructType): StoreIterator[InternalRow] = stores(shard).scan(projections)

  def close(): Unit = stores.foreach(_.close())

}

object Store {

  object MetadataColumns {
    object ShardId {
      val Name: String = "_shard_id"
      val Ordinal: Int = 0
      val Type: DataType = DataTypes.IntegerType
      val Field: StructField = StructField(Name, Type, nullable = false)
    }

    object RowId{
      val Name: String = "_row_id"
      val Ordinal: Int = 1
      val Type: DataType = DataTypes.BinaryType
      val Field: StructField = StructField(Name, Type, nullable = false)
      def get(row: InternalRow): Array[Byte] = row.getBinary(Ordinal)
    }
  }

  trait Store {
    val name: String
    val schema: StructType
    val `type`: Types.Type
    def insert(row: InternalRow): Unit
    def insert(rows: Iterator[InternalRow]): Unit
    def scan(projections: StructType): StoreIterator[InternalRow]
    def close(): Unit
  }

  trait StoreIterator[T] extends Iterator[T] with Closeable

  private class InMemoryStore(override val name: String,
                              override val schema: StructType,
                              override val `type`: Types.Type) extends Store { store =>

    private val recordManager = RecordManager(name, schema, projections = StructType(Seq()))

    private val writer = recordManager.writer()

    private val map = new Object2ObjectOpenHashMap[UUID, Array[Byte]]()

    override def insert(row: InternalRow): Unit = map.synchronized {
      map.put(UuidCreator.fromBytes(MetadataColumns.RowId.get(row)), writer(row))
    }

    override def insert(rows: Iterator[InternalRow]): Unit = map.synchronized {
      map.putAll(rows.map(row => UuidCreator.fromBytes(MetadataColumns.RowId.get(row)) -> writer(row)).toMap.asJava)
    }

    override def scan(projections: StructType): StoreIterator[InternalRow] = {
      new StoreIterator[InternalRow] {
        private val reader = {
          if (projections.isEmpty) store.recordManager.reader()
          else RecordManager(name, schema, projections).reader()
        }

        private val underlying = store.map.values().iterator().asScala
        override def hasNext: Boolean = underlying.hasNext
        override def next(): InternalRow = reader(underlying.next())
        override def close(): Unit = ()
      }
    }

    override def close(): Unit = ()

  }

  def apply(name: String, schema: StructType, `type`: Types.Type): Store = {
    `type` match {
      case Types.InMemory => new InMemoryStore(name, schema, `type`)
      case Types.RocksDB => RocksDBStoreManager.get().initialize(name, schema)
    }
  }

  def sharded(name: String, schema: StructType, shards: Int, `type`: Types.Type): ShardedStore = {
//    require(
//      schema(MetadataColumns.ShardId.Ordinal).name == MetadataColumns.ShardId.Name,
//      s"the schema must contain a column named '${MetadataColumns.ShardId.Name}' at position ${MetadataColumns.ShardId.Ordinal}"
//    )
//
//    require(
//      schema(MetadataColumns.RowId.Ordinal).name == MetadataColumns.RowId.Name,
//      s"the schema must contain a column named '${MetadataColumns.RowId.Name}' at position ${MetadataColumns.RowId.Ordinal}"
//    )

    val stores = (0 until shards).map { shard =>
      Store(s"${name}__$shard", schema, `type`)
    }.toArray

    new ShardedStore(stores)
  }

  def load(shards: Seq[Store]): ShardedStore = new ShardedStore(shards.toArray)

  def toStoreSchema(schema: StructType): StructType = {
    if (schema.fields.exists(field => field.name == MetadataColumns.ShardId.Name)) schema
    else StructType(Seq(MetadataColumns.ShardId.Field, MetadataColumns.RowId.Field) ++ schema)
  }

  def toStoreRow(row: InternalRow, schema: StructType, router: ShardRouter): InternalRow = {
    val uuid = UuidCreator.getTimeOrdered
    InternalRow.fromSeq(Seq(router.assign(uuid), UuidCreator.toBytes(uuid)) ++ row.copy().toSeq(schema))
  }

}
