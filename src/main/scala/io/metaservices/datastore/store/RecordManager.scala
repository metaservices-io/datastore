package io.metaservices.datastore.store

import io.metaservices.datastore.codegen.RecordEncoderUtils
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.types.StructType

class RecordManager(name: String, schema: StructType, projections: StructType) {

  private val encoder = RecordEncoderUtils.generate(name, schema, projections)

  def writer(): InternalRow => Array[Byte] = { row => encoder.encode(row) }

  def reader(): Array[Byte] => InternalRow = { bytes => encoder.decode(bytes) }

}

object RecordManager {

  def apply(name: String, schema: StructType, projections: StructType): RecordManager = {
    new RecordManager(name, schema, projections)
  }

}
