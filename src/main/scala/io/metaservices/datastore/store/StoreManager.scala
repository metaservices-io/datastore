package io.metaservices.datastore.store

import io.metaservices.datastore.store.Store.Store
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap
import org.apache.spark.sql.types.StructType

import scala.jdk.CollectionConverters.IterableHasAsScala

object StoreManager {

  private val stores = new Object2ObjectOpenHashMap[String, ShardedStore]()

  sys.addShutdownHook(stores.values.asScala.foreach(_.close()))

  object Types {

    sealed trait Type {
      val name: String
    }

    case object InMemory extends Type {
      override val name: String = "in-memory"
    }

    case object RocksDB extends Type {
      override val name: String = "rocksdb"
    }

    def of(name: String): Type = {
      name match {
        case InMemory.name => InMemory
        case RocksDB.name => RocksDB
      }
    }

  }

  def get(name: String): Option[ShardedStore] = Option(stores.get(name))

  def create(name: String, schema: StructType, shards: Int, `type`: Types.Type): Unit = this.synchronized {
    if (!stores.containsKey(name)) stores.put(name, initialize(name, schema, shards, `type`))
  }

  def load(shards: Seq[Store]): ShardedStore = {
    val store = Store.load(shards)
    stores.put(store.name, store)

    store
  }

  private def initialize(name: String, schema: StructType, shards: Int, `type`: Types.Type): ShardedStore = {
    Store.sharded(name, schema, shards, `type`)
  }

}
