package io.metaservices.datastore.store.rocksdb

import com.google.common.primitives.{Bytes, Longs}
import io.metaservices.datastore.store.RecordManager
import io.metaservices.datastore.store.Store.{MetadataColumns, Store, StoreIterator}
import io.metaservices.datastore.store.StoreManager.Types
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.types.StructType

class RocksDBStore(override val name: String,
                   override val schema: StructType,
                   val id: Long,
                   rocksDBManager: RocksDBStoreManager) extends Store { store =>

  override val `type`: Types.Type = Types.RocksDB

  private val prefix: Array[Byte] = Longs.toByteArray(id)

  private val recordManager = RecordManager(name, schema, projections = StructType(Seq()))

  private val writer = recordManager.writer()

  override def insert(row: InternalRow): Unit = {
    rocksDBManager.write(Bytes.concat(prefix, MetadataColumns.RowId.get(row)), writer(row))
  }

  override def insert(rows: Iterator[InternalRow]): Unit = {
    rocksDBManager.write(rows.map(row => Bytes.concat(prefix, MetadataColumns.RowId.get(row)) -> writer(row)))
  }

  override def scan(projections: StructType): StoreIterator[InternalRow] = {
    new StoreIterator[InternalRow] {
      private val reader = {
        if (projections.isEmpty) store.recordManager.reader()
        else RecordManager(name, schema, projections).reader()
      }

      private val underlying = rocksDBManager.iterator(id)
      override def hasNext: Boolean = underlying.hasNext
      override def next(): InternalRow = reader(underlying.next()._2)
      override def close(): Unit = underlying.close()
    }
  }

  override def close(): Unit = ()

}
