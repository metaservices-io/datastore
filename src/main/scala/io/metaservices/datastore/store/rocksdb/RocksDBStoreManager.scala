package io.metaservices.datastore.store.rocksdb

import better.files.File
import com.google.common.collect.Lists
import com.google.common.primitives.Longs
import com.twitter.util.StorageUnit
import io.metaservices.datastore.store.RecordManager
import io.metaservices.datastore.store.Store.StoreIterator
import io.metaservices.datastore.store.rocksdb.RocksDBStoreManager.{metadataColumnFamilyName, metadataRecordManager, metadataRecordSchema, rowsColumnFamilyName}
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.apache.spark.unsafe.types.UTF8String
import org.rocksdb._

import java.nio.charset.StandardCharsets
import java.util.concurrent.atomic.AtomicLong
import scala.collection.concurrent.TrieMap
import scala.collection.mutable.ArrayBuffer
import scala.jdk.CollectionConverters.{CollectionHasAsScala, SeqHasAsJava}

class RocksDBStoreManager(directory: File) {

  private val options = new DBOptions()
    .setAllowConcurrentMemtableWrite(false)
    .setAllow2pc(true)
    .setAllowMmapWrites(true)
    .setAllowMmapReads(true)
    .setCreateIfMissing(true)
    .setCreateMissingColumnFamilies(true)
    .setIncreaseParallelism(Runtime.getRuntime.availableProcessors())
    .setMaxOpenFiles(-1)
    .setMaxBackgroundJobs(2)

  private val columnFamilyOptions = new ColumnFamilyOptions()
    .setBottommostCompressionType(CompressionType.LZ4_COMPRESSION)
    .setBlobCompressionType(CompressionType.LZ4_COMPRESSION)
    .setCompactionStyle(CompactionStyle.LEVEL)
    .setLevelZeroFileNumCompactionTrigger(10)
    .setLevelZeroSlowdownWritesTrigger(20)
    .setLevelZeroStopWritesTrigger(40)
    .setWriteBufferSize(StorageUnit.fromMegabytes(64).bytes)
    .setTargetFileSizeBase(StorageUnit.fromMegabytes(64).bytes)
    .setMaxBytesForLevelBase(StorageUnit.fromMegabytes(512).bytes)
    .setEnableBlobFiles(true)
    .useFixedLengthPrefixExtractor(Longs.BYTES)
    .setMemtablePrefixBloomSizeRatio(0.1)
    .setMemTableConfig(new HashSkipListMemTableConfig())
    .setTableFormatConfig(
      new BlockBasedTableConfig()
        .setBlockSize(StorageUnit.fromKilobytes(4).bytes)
        .setIndexType(IndexType.kHashSearch)
        .setBlockCache(new LRUCache(StorageUnit.fromMegabytes(512).bytes))
    )

  private val columnFamilyDescriptors = Map(
    new String(RocksDB.DEFAULT_COLUMN_FAMILY, StandardCharsets.US_ASCII) ->
      new ColumnFamilyDescriptor(RocksDB.DEFAULT_COLUMN_FAMILY),
    RocksDBStoreManager.metadataColumnFamilyName ->
      new ColumnFamilyDescriptor(RocksDBStoreManager.metadataColumnFamilyName.getBytes(StandardCharsets.US_ASCII)),
    RocksDBStoreManager.rowsColumnFamilyName ->
      new ColumnFamilyDescriptor(
        RocksDBStoreManager.rowsColumnFamilyName.getBytes(StandardCharsets.US_ASCII),
        columnFamilyOptions
      )
  )

  private val columnFamilyHandles = TrieMap.empty[String, ColumnFamilyHandle]

  private val rocksDB = {
    directory.createDirectoryIfNotExists(createParents = true)
    val handles = Lists.newArrayList[ColumnFamilyHandle]()
    val instance = RocksDB.open(options, directory.pathAsString, columnFamilyDescriptors.values.toList.asJava, handles)
    handles.asScala.foreach { handle =>
      columnFamilyHandles += new String(handle.getName, StandardCharsets.US_ASCII) -> handle
    }

    instance
  }

  private val metadataRecordReader = metadataRecordManager.reader()

  private val metadataRecordWriter = metadataRecordManager.writer()

  private val storeIdGenerator = new AtomicLong(0)

  def load(): Seq[RocksDBStore] = {
    val rocksIterator = rocksDB.newIterator(columnFamilyHandles(metadataColumnFamilyName))
    rocksIterator.seekToFirst()

    try {
      val stores = ArrayBuffer.empty[RocksDBStore]
      while (rocksIterator.isValid) {
        val metadata = metadataRecordReader(rocksIterator.value())
        stores += new RocksDBStore(
          id = metadata.getLong(metadataRecordSchema.fieldIndex("id")),
          name = metadata.getString(metadataRecordSchema.fieldIndex("name")),
          schema = StructType.fromDDL(metadata.getString(metadataRecordSchema.fieldIndex("schema"))),
          rocksDBManager = this
        )

        rocksIterator.next()
      }

      if (stores.nonEmpty) storeIdGenerator.set(stores.map(_.id).max)
      stores.toSeq
    } finally rocksIterator.close()
  }

  def initialize(name: String, schema: StructType): RocksDBStore = {
    val id = storeIdGenerator.getAndIncrement()
    val metadata = InternalRow(id, UTF8String.fromString(name), UTF8String.fromString(schema.toDDL))
    rocksDB.put(columnFamilyHandles(metadataColumnFamilyName), name.getBytes(StandardCharsets.UTF_8), metadataRecordWriter(metadata))

    new RocksDBStore(name, schema, id, rocksDBManager = this)
  }

  def write(key: Array[Byte], value: Array[Byte]): Unit = {
    rocksDB.put(columnFamilyHandles(rowsColumnFamilyName), key, value)
  }

  def write(entries: Iterator[(Array[Byte], Array[Byte])]): Unit = {
    val handler = columnFamilyHandles(rowsColumnFamilyName)
    val writeOptions = new WriteOptions()
    val writeBatch = new WriteBatch()

    try {
      while (entries.hasNext) {
        val (key, value) = entries.next()
        writeBatch.put(handler, key, value)
      }

      rocksDB.write(writeOptions, writeBatch)
    } finally {
      writeBatch.close()
      writeOptions.close()
    }
  }

  def iterator(prefix: Long): StoreIterator[(Array[Byte], Array[Byte])] = {
    val start = Longs.toByteArray(prefix)
    val end = Longs.toByteArray(prefix + 1)

    new StoreIterator[(Array[Byte], Array[Byte])] {
      private val snapshot = rocksDB.getSnapshot
      private val readOptions = new ReadOptions()
        .setPrefixSameAsStart(true)
        .setIterateLowerBound(new Slice(start))
        .setIterateUpperBound(new Slice(end))
        .setSnapshot(snapshot)

      private val rocksIterator = rocksDB.newIterator(columnFamilyHandles(rowsColumnFamilyName), readOptions)
      rocksIterator.seek(start)

      override def hasNext: Boolean = rocksIterator.isValid
      override def next(): (Array[Byte], Array[Byte]) = {
        val entry = rocksIterator.key() -> rocksIterator.value()
        rocksIterator.next()

        entry
      }

      override def close(): Unit = {
        rocksIterator.close()
        readOptions.close()
        rocksDB.releaseSnapshot(snapshot)
        snapshot.close()
      }
    }
  }

  def close(): Unit = {
    rocksDB.close()
    options.close()
    columnFamilyHandles.values.foreach(_.close())
    columnFamilyOptions.close()
  }

}

object RocksDBStoreManager {

  RocksDB.loadLibrary()

  private val metadataColumnFamilyName: String = "metadata"

  private val rowsColumnFamilyName: String = "rows"

  private val metadataRecordSchema = StructType(Seq(
    StructField("id", DataTypes.LongType, nullable = false),
    StructField("name", DataTypes.StringType, nullable = false),
    StructField("schema", DataTypes.StringType, nullable = false)
  ))

  private val metadataRecordManager = RecordManager(
    metadataColumnFamilyName, metadataRecordSchema, projections = StructType(Seq())
  )

  private var instance: RocksDBStoreManager = _

  def load(directory: File): Seq[RocksDBStore] = this.synchronized {
    if (instance == null) instance = new RocksDBStoreManager(directory)
    sys.addShutdownHook(instance.close())
    instance.load()
  }

  def get(): RocksDBStoreManager = instance

}
