package io.metaservices.datastore.store

import io.github.ykayacan.hashing.api.HashFunction
import io.github.ykayacan.hashing.rendezvous.strategy.DefaultRendezvousStrategy
import io.github.ykayacan.hashing.rendezvous.{RendezvousNodeRouter, WeightedNode}
import io.metaservices.datastore.store.ShardRouter.XXHashFunction
import net.openhft.hashing.LongHashFunction

import java.util.UUID

class ShardRouter(shards: Int) {

  private val router = {
    val instance = RendezvousNodeRouter.create[WeightedNode](XXHashFunction(), DefaultRendezvousStrategy.create[WeightedNode]())
    (0 until shards).foreach(shard => instance.addNode(WeightedNode.of(s"$shard")))
    instance
  }

  def assign(rowId: UUID): Int = {
    router.getNode(rowId.toString).get().getNodeId.toInt
  }

}

object ShardRouter {

  def apply(shards: Int): ShardRouter = new ShardRouter(shards)

  private case class XXHashFunction() extends HashFunction {
    override def hash(key: String): Long = LongHashFunction.xx3().hashChars(key)
  }

}
