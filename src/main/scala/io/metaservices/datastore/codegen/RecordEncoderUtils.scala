package io.metaservices.datastore.codegen

import com.google.common.primitives.{Ints, Longs}
import com.google.googlejavaformat.java.Formatter
import io.metaservices.datastore.codegen.CodeGenClassLoaderHelper.RestrictedClassLoader
import net.openhft.hashing.LongHashFunction
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}

import java.util.concurrent.locks.ReentrantLock

object RecordEncoderUtils {

  private val mutex = new ReentrantLock()

  private def copyIntValueToOffsets(index: Int): String = {
    s"""
       |offsets[${index * (Longs.BYTES + 1) + 5}] = value[0];
       |offsets[${index * (Longs.BYTES + 1) + 6}] = value[1];
       |offsets[${index * (Longs.BYTES + 1) + 7}] = value[2];
       |offsets[${index * (Longs.BYTES + 1) + 8}] = value[3];
     """.stripMargin
  }

  private def copyLongValueToOffsets(index: Int): String = {
    s"""
       |offsets[${index * (Longs.BYTES + 1) + 1}] = value[0];
       |offsets[${index * (Longs.BYTES + 1) + 2}] = value[1];
       |offsets[${index * (Longs.BYTES + 1) + 3}] = value[2];
       |offsets[${index * (Longs.BYTES + 1) + 4}] = value[3];
       |offsets[${index * (Longs.BYTES + 1) + 5}] = value[4];
       |offsets[${index * (Longs.BYTES + 1) + 6}] = value[5];
       |offsets[${index * (Longs.BYTES + 1) + 7}] = value[6];
       |offsets[${index * (Longs.BYTES + 1) + 8}] = value[7];
     """.stripMargin
  }

  private def copyHeapPositionToOffsets(index: Int): String = {
    s"""
       |offsets[${index * (Longs.BYTES + 1) + 5}] = heapPosition[0];
       |offsets[${index * (Longs.BYTES + 1) + 6}] = heapPosition[1];
       |offsets[${index * (Longs.BYTES + 1) + 7}] = heapPosition[2];
       |offsets[${index * (Longs.BYTES + 1) + 8}] = heapPosition[3];
     """.stripMargin
  }

  private val defaultCaseInOrdinalSwitch = """default: throw new RuntimeException("invalid field position: " + ordinal);"""

  def generate(name: String,
               schema: StructType,
               projections: StructType = StructType(Seq()),
               classLoader: RestrictedClassLoader = CodeGenClassLoaderHelper(CodeGenClassLoaderHelper.Shared).getClassLoader): RecordEncoder = {

    val projected = project(schema, projections)
    val version = java.lang.Long.toUnsignedString(LongHashFunction.xx3().hashChars(schema.sql ++ projections.sql))
    val className = s"${StringUtils.capitalize(name)}RecordEncoderV$version"
    val fullName = s"${CodeGenClassLoaderHelper.PackageName}.$className"
    val clazz = {
      classLoader.tryLoadClass(fullName) match {
        case Some(existing) => existing.asInstanceOf[Class[RecordEncoder]]
        case None =>
          mutex.lock()
          try {
            classLoader
              .tryLoadClass(fullName)
              .getOrElse(generateClass(className, schema, projected, classLoader))
              .asInstanceOf[Class[RecordEncoder]]
          } finally mutex.unlock()
      }
    }

    clazz.getDeclaredConstructor().newInstance()
  }

  private def generateClass(className: String,
                            schema: StructType,
                            projected: Array[(Int, StructField)],
                            classLoader: RestrictedClassLoader): Class[RecordEncoder] = {

    val indexed = projected.zipWithIndex
    val heapOffset = schema.size * (Longs.BYTES + 1)
    val recordInternalRowClassName = s"${className}InternalRow"
    val recordInternalRowBody =
      s"""
         |package ${CodeGenClassLoaderHelper.PackageName};
         |
         |import io.metaservices.datastore.codegen.RecordInternalRow;
         |import org.apache.spark.unsafe.types.UTF8String;
         |import org.apache.spark.unsafe.types.CalendarInterval;
         |import org.apache.spark.sql.catalyst.InternalRow;
         |import com.google.common.primitives.Ints;
         |import com.google.common.primitives.Shorts;
         |import com.google.common.primitives.Longs;
         |
         |final class ${className}InternalRow extends RecordInternalRow {
         |  public ${className}InternalRow(final byte[] bytes) {
         |    super(bytes);
         |  }
         |
         |  @Override
         |  public int numFields() {
         |    return ${schema.size};
         |  }
         |
         |  @Override
         |  public InternalRow copy() {
         |    return new ${className}InternalRow(bytes);
         |  }
         |
         |  @Override
         |  public boolean isNullAt(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateIsNullAtCases(indexed)}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public boolean getBoolean(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetBooleanCases(indexed.filter(_._1._2.dataType == DataTypes.BooleanType))}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public byte getByte(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetByteCases(indexed.filter(_._1._2.dataType == DataTypes.ByteType))}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public short getShort(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetShortCases(indexed.filter(_._1._2.dataType == DataTypes.ShortType))}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public int getInt(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetIntCases(indexed.filter { case ((_, field), _) => field.dataType == DataTypes.IntegerType || field.dataType == DataTypes.DateType })}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public long getLong(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetLongCases(indexed.filter { case ((_, field), _) => field.dataType == DataTypes.LongType || field.dataType == DataTypes.TimestampType })}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public float getFloat(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetFloatCases(indexed.filter(_._1._2.dataType == DataTypes.FloatType))}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public double getDouble(final int ordinal) {
         |    switch (ordinal) {
         |      ${generateGetDoubleCases(indexed.filter(_._1._2.dataType == DataTypes.DoubleType))}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public UTF8String getUTF8String(final int ordinal) {
         |    int valueOffset = 0;
         |    int valueLength = 0;
         |    switch (ordinal) {
         |      ${generateGetUTF8StringCases(indexed.filter(_._1._2.dataType == DataTypes.StringType), heapOffset)}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public byte[] getBinary(final int ordinal) {
         |    int valueOffset = 0;
         |    int valueLength = 0;
         |    int valueStart = 0;
         |    switch (ordinal) {
         |      ${generateGetBinaryCases(indexed.filter(_._1._2.dataType == DataTypes.BinaryType), heapOffset)}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |
         |  @Override
         |  public CalendarInterval getInterval(final int ordinal) {
         |    int valueOffset = 0;
         |    int months = 0;
         |    int days = 0;
         |    long microseconds = 0;
         |    switch (ordinal) {
         |      ${generateGetIntervalCases(indexed.filter(_._1._2.dataType == DataTypes.CalendarIntervalType), heapOffset)}
         |      $defaultCaseInOrdinalSwitch
         |    }
         |  }
         |}
       """.stripMargin

    val recordInternalRowBodyFormatted = new Formatter().formatSource(recordInternalRowBody)
    classLoader.compile(recordInternalRowClassName, recordInternalRowBodyFormatted)

    val body =
      s"""
         |package ${CodeGenClassLoaderHelper.PackageName};
         |
         |import io.metaservices.datastore.codegen.RecordEncoder;
         |import org.apache.spark.sql.catalyst.InternalRow;
         |import org.apache.spark.sql.types.StructType;
         |import org.apache.spark.unsafe.types.CalendarInterval;
         |import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
         |import com.google.common.primitives.Bytes;
         |import com.google.common.primitives.Ints;
         |import com.google.common.primitives.Longs;
         |import com.google.common.primitives.Shorts;
         |
         |final public class $className extends RecordEncoder {
         |  public $className() {
         |  }
         |
         |  @Override
         |  public byte[] encode(final InternalRow row) throws java.io.IOException {
         |    final byte[] offsets = new byte[${schema.size * (Longs.BYTES + 1)}];
         |    final FastByteArrayOutputStream heap = new FastByteArrayOutputStream();
         |    byte[] heapPosition = null;
         |    byte[] value = null;
         |    ${generateEncodeBody(schema)}
         |    heap.trim();
         |
         |    final byte[] bytes = new byte[offsets.length + heap.length];
         |    return Bytes.concat(offsets, heap.array);
         |  }
         |
         |  @Override
         |  public InternalRow decode(final byte[] bytes) throws java.io.IOException {
         |    return new $recordInternalRowClassName(bytes);
         |  }
         |}
       """.stripMargin

    val bodyFormatted = new Formatter().formatSource(body)
    classLoader.compile(className, bodyFormatted).asInstanceOf[Class[RecordEncoder]]
  }

  private def generateEncodeBody(schema: StructType): String = {
    schema
      .fields
      .zipWithIndex
      .map { case (field, index) =>
        s"""
           |heapPosition = Ints.toByteArray(heap.length);
           |if (row.isNullAt($index)) {
           |  offsets[${index * (Longs.BYTES + 1)}] = 1;
           |} else {
           |  offsets[${index * (Longs.BYTES + 1)}] = 0;
           |  ${generateEncodeFieldPart(field, index)}
           |}
         """.stripMargin
      }
      .mkString("\n")
  }

  private def generateEncodeFieldPart(field: StructField, index: Int): String = {
    field.dataType match {
      case DataTypes.BooleanType
           | DataTypes.DateType
           | DataTypes.TimestampType
           | DataTypes.DoubleType
           | DataTypes.FloatType
           | DataTypes.ByteType
           | DataTypes.IntegerType
           | DataTypes.LongType
           | DataTypes.ShortType
           | DataTypes.NullType => generateEncodeInlinedType(field, index)
      case _ => generateEncodeComplexType(field, index)
    }
  }

  private def generateEncodeInlinedType(field: StructField, index: Int): String = {
    field.dataType match {
      case DataTypes.NullType => ""
      case DataTypes.BooleanType =>
        s"""
           |offsets[${index * (Longs.BYTES + 1) + 8}] = row.getBoolean($index) ? (byte) 1 : (byte) 0;
         """.stripMargin

      case DataTypes.ByteType =>
        s"""
           |offsets[${index * (Longs.BYTES + 1) + 8}] = row.getByte($index);
         """.stripMargin

      case DataTypes.ShortType =>
        s"""
           |value = Shorts.toByteArray(row.getShort($index));
           |offsets[${index * (Longs.BYTES + 1) + 7}] = value[0];
           |offsets[${index * (Longs.BYTES + 1) + 8}] = value[1];
         """.stripMargin

      case DataTypes.IntegerType | DataTypes.DateType =>
        s"""
           |value = Ints.toByteArray(row.getInt($index));
           |${copyIntValueToOffsets(index)}
         """.stripMargin

      case DataTypes.LongType | DataTypes.TimestampType =>
        s"""
           |value = Longs.toByteArray(row.getLong($index));
           |${copyLongValueToOffsets(index)}
         """.stripMargin

      case DataTypes.FloatType =>
        s"""
           |value = Ints.toByteArray(Float.floatToIntBits(row.getFloat($index)));
           |${copyIntValueToOffsets(index)}
         """.stripMargin

      case DataTypes.DoubleType =>
        s"""
           |value = Longs.toByteArray(Double.doubleToLongBits(row.getDouble($index)));
           |${copyLongValueToOffsets(index)}
         """.stripMargin
    }
  }

  private def generateEncodeComplexType(field: StructField, index: Int): String = {
    val heapEncoder = {
      field.dataType match {
        case DataTypes.CalendarIntervalType =>
          s"""
             |{
             |  final CalendarInterval interval = row.getInterval($index);
             |  heap.write(Ints.toByteArray(interval.months));
             |  heap.write(Ints.toByteArray(interval.days));
             |  heap.write(Longs.toByteArray(interval.microseconds));
             |}
           """.stripMargin

        case DataTypes.StringType =>
          s"""
             |value = row.getUTF8String($index).getBytes();
             |heap.write(Ints.toByteArray(value.length));
             |heap.write(value);
           """.stripMargin

        case DataTypes.BinaryType =>
          s"""
             |value = row.getBinary($index);
             |heap.write(Ints.toByteArray(value.length));
             |heap.write(value);
           """.stripMargin
      }
    }

    s"""
       |${copyHeapPositionToOffsets(index)}
       |$heapEncoder
     """.stripMargin
  }

  private def generateIsNullAtCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, field), index) =>
        field.dataType match {
          case DataTypes.NullType => s"case $index: return true;"
          case _ => s"case $index: return bytes[${original * (Longs.BYTES + 1)}] == 1;"
        }
      }
      .mkString("\n")
  }

  private def generateGetBooleanCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return bytes[${original * (Longs.BYTES + 1) + 8}] == 1;"
      }
      .mkString("\n")
  }

  private def generateGetByteCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return bytes[${original * (Longs.BYTES + 1) + 8}];"
      }
      .mkString("\n")
  }

  private def generateGetShortCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return Shorts.fromBytes(bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]);"
      }
      .mkString("\n")
  }

  private def generateGetIntCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return Ints.fromBytes(bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]);"
      }
      .mkString("\n")
  }

  private def generateGetLongCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return Longs.fromBytes(bytes[${original * (Longs.BYTES + 1) + 1}], bytes[${original * (Longs.BYTES + 1) + 2}], bytes[${original * (Longs.BYTES + 1) + 3}], bytes[${original * (Longs.BYTES + 1) + 4}], bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]);"
      }
      .mkString("\n")
  }

  private def generateGetFloatCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return Float.intBitsToFloat(Ints.fromBytes(bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]));"
      }
      .mkString("\n")
  }

  private def generateGetDoubleCases(projected: Array[((Int, StructField), Int)]): String = {
    projected
      .map { case ((original, _), index) =>
        s"case $index: return Double.longBitsToDouble(Longs.fromBytes(bytes[${original * (Longs.BYTES + 1) + 1}], bytes[${original * (Longs.BYTES + 1) + 2}], bytes[${original * (Longs.BYTES + 1) + 3}], bytes[${original * (Longs.BYTES + 1) + 4}], bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]));"
      }
      .mkString("\n")
  }

  private def generateGetUTF8StringCases(projected: Array[((Int, StructField), Int)], heapOffset: Int): String = {
    projected
      .map { case ((original, _), index) =>
        s"""
           |case $index:
           |  valueOffset = $heapOffset + Ints.fromBytes(bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]);
           |  valueLength = Ints.fromBytes(bytes[valueOffset], bytes[valueOffset + 1], bytes[valueOffset + 2], bytes[valueOffset + 3]);
           |  return UTF8String.fromBytes(bytes, valueOffset + ${Ints.BYTES}, valueLength);
         """.stripMargin
      }
      .mkString("\n")
  }

  private def generateGetBinaryCases(projected: Array[((Int, StructField), Int)], heapOffset: Int): String = {
    projected
      .map { case ((original, _), index) =>
        s"""
           |case $index:
           |  valueOffset = $heapOffset + Ints.fromBytes(bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]);
           |  valueLength = Ints.fromBytes(bytes[valueOffset], bytes[valueOffset + 1], bytes[valueOffset + 2], bytes[valueOffset + 3]);
           |  valueStart = valueOffset + ${Ints.BYTES};
           |  return java.util.Arrays.copyOfRange(bytes, valueStart, valueStart + valueLength);
         """.stripMargin
      }
      .mkString("\n")
  }

  private def generateGetIntervalCases(projected: Array[((Int, StructField), Int)], heapOffset: Int): String = {
    projected
      .map { case ((original, _), index) =>
        s"""
           |case $index:
           |  valueOffset = $heapOffset + Ints.fromBytes(bytes[${original * (Longs.BYTES + 1) + 5}], bytes[${original * (Longs.BYTES + 1) + 6}], bytes[${original * (Longs.BYTES + 1) + 7}], bytes[${original * (Longs.BYTES + 1) + 8}]);
           |  months = Ints.fromBytes(bytes[valueOffset], bytes[valueOffset + 1], bytes[valueOffset + 2], bytes[valueOffset + 3]);
           |  days = Ints.fromBytes(bytes[valueOffset + 4], bytes[valueOffset + 5], bytes[valueOffset + 6], bytes[valueOffset + 7]);
           |  microseconds = Longs.fromBytes(bytes[valueOffset + 8], bytes[valueOffset + 9], bytes[valueOffset + 10], bytes[valueOffset + 11], bytes[valueOffset + 12], bytes[valueOffset + 13], bytes[valueOffset + 14], bytes[valueOffset + 15]);
           |  return new CalendarInterval(months, days, microseconds);
         """.stripMargin
      }
      .mkString("\n")
  }

  private def project(schema: StructType, projections: StructType): Array[(Int, StructField)] = {
    if (projections.isEmpty) schema.zipWithIndex.map { case (field, index) => index -> field }.toArray
    else projections.map { field => schema.fieldIndex(field.name) -> field }.toArray
  }

}
