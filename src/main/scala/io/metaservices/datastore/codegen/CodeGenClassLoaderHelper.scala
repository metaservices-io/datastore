package io.metaservices.datastore.codegen

import io.metaservices.datastore.codegen.CodeGenClassLoaderHelper.{RestrictedClassLoader, SynchronizedRestrictedClassLoader, UnsafeRestrictedClassLoader}
import org.codehaus.janino.{ClassLoaderIClassLoader, Parser, Scanner, UnitCompiler}

import java.io.StringReader
import java.security.{Permissions, ProtectionDomain, SecureClassLoader}

sealed trait CodeGenClassLoaderHelper {
  def getClassLoader: RestrictedClassLoader
}

private class CodeGenClassLoaderHelperThreadLocal extends CodeGenClassLoaderHelper {
  private val classLoaders: ThreadLocal[RestrictedClassLoader] = new ThreadLocal[RestrictedClassLoader]()
  override def getClassLoader: RestrictedClassLoader = {
    if (classLoaders.get() == null) {
      classLoaders.set(new UnsafeRestrictedClassLoader(Thread.currentThread().getContextClassLoader))
    }

    classLoaders.get()
  }
}

private class CodeGenClassLoaderHelperShared extends CodeGenClassLoaderHelper {
  private val classLoader: RestrictedClassLoader = new SynchronizedRestrictedClassLoader(
    new UnsafeRestrictedClassLoader(ClassLoader.getSystemClassLoader)
  )

  override def getClassLoader: RestrictedClassLoader = classLoader
}

object CodeGenClassLoaderHelper {

  val PackageName: String = "io.metaservices.datastore.codegen.runtime"

  private val shared: CodeGenClassLoaderHelper = new CodeGenClassLoaderHelperShared

  private val threadLocal: CodeGenClassLoaderHelper = new CodeGenClassLoaderHelperThreadLocal

  sealed trait Mod {
    val name: String
  }

  case object Shared extends Mod {
    override val name: String = "shared"
  }

  case object ThreadLocal extends Mod {
    override val name: String = "thread-local"
  }

  def apply(mod: Mod): CodeGenClassLoaderHelper = {
    mod match {
      case Shared => shared
      case ThreadLocal => threadLocal
    }
  }

  def apply(modName: String): CodeGenClassLoaderHelper = {
    modName match {
      case Shared.name => shared
      case ThreadLocal.name => threadLocal
    }
  }

  sealed trait RestrictedClassLoader {
    def compile(className: String, classBody: String): Class[_]
    def tryLoadClass(name: String): Option[Class[_]]
  }

  final class UnsafeRestrictedClassLoader(private val parentClassLoader: ClassLoader) extends SecureClassLoader(parentClassLoader) with RestrictedClassLoader {

    override def compile(className: String, classBody: String): Class[_] = {
      val unitCompiler = new UnitCompiler(
        new Parser(new Scanner(null, new StringReader(classBody))).parseAbstractCompilationUnit(),
        new ClassLoaderIClassLoader(this)
      )

      val classFiles = unitCompiler.compileUnit(false, false, false)
      val classBytes = classFiles(0).toByteArray
      defineClass(className, classBytes)
    }

    override def tryLoadClass(name: String): Option[Class[_]] = {
      try Some(this.loadClass(name))
      catch {
        case _: ClassNotFoundException => None
      }
    }

    private def defineClass(name: String, bytes: Array[Byte]): Class[_] = {
      defineClass(s"$PackageName.$name", bytes, 0, bytes.length, new ProtectionDomain(null, new Permissions, this, null))
    }

  }

  final class SynchronizedRestrictedClassLoader(private val unsafeRestrictedClassLoader: UnsafeRestrictedClassLoader) extends SecureClassLoader(unsafeRestrictedClassLoader) with RestrictedClassLoader {
    override def compile(className: String, classBody: String): Class[_] = unsafeRestrictedClassLoader.synchronized {
      unsafeRestrictedClassLoader.compile(className, classBody)
    }

    override def tryLoadClass(name: String): Option[Class[_]] = unsafeRestrictedClassLoader.tryLoadClass(name)
  }

}
